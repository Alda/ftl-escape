<?php

require_once(__DIR__."/bootstrap.php");
require_once __DIR__."/const.php";

class Tools {

    static function system_hack($info=array())
    {
?>
        <html>
            <head>
                <title>Hack detected</title>
            </head>
            <body>
                It appears you tried to hack the system. The incident has been reported.<br />
                <strong>Your account is now desactivated for security reasons.</strong><br />
                If you didn't attempt to corrupt the system, feel free to contact us.
            </body>
        </html>
<?php
        exit(42);
    }

    static function login_exists($login)
	{
		global $entityManager;
		$test = $entityManager->getRepository('Player')->findOneByLogin($login);
		return !is_null($test);
	}

	static function gameOver($player)
	{
		global $entityManager;
		$player->gameOver();
		$entityManager->flush();
	}

	static function getFreeRandomCoordinates($sourcex=0,$sourcey=0,$sourcez=0,$size=MAP_SIZE)
	{
		global $entityManager;
		$x=rand($sourcex-$size,$sourcex+$size);
		$y=rand($sourcey-$size,$sourcey+$size);
		$z=0;
		$repository = $entityManager->getRepository('Planet');
		$planet = $repository->findOneBy(array('x'=>$x,'y'=>$y,'z'=>$z));
		$repositoryfleet = $entityManager->getRepository('Fleet');
		$fleet = $repositoryfleet->findOneBy(array('x'=>$x,'y'=>$y,'z'=>$z));
		while (!is_null($planet) || !is_null($fleet))
		{
			$x=rand($sourcex-$size,$sourcex+$size);
			$y=rand($sourcey-$size,$sourcey+$size);
			$z=0;
			$planet = $repository->findOneBy(array('x'=>$x,'y'=>$y,'z'=>$z));
			$fleet = $repositoryfleet->findOneBy(array('x'=>$x,'y'=>$y,'z'=>$z));
			echo "Testing $x/$y/$z";
		}
		return array('x'=>$x,'y'=>$y,'z'=>$z);
	}

    static function randomString($car)
    {
        $string = "";
        $chaine = 'abcdefghijklmnpqrstuvwxyABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        srand((double)microtime()*1000000);
        for($i=0; $i<$car; $i++) {
            $string .= $chaine[rand()%strlen($chaine)];
        }
        return $string;
    }

    static function remove_accents($str, $charset='UTF-8')
    {
        $str = htmlentities($str, ENT_NOQUOTES, $charset, false);
        return $str;
    }

    static function enum($array, $asBitwise = FALSE) {

            if($array === null)             return FALSE;
            if(!is_array($array))           return FALSE;

            $count = 0;

            foreach($array as $i):
                if($i === null):
                    if($count == 0)      define($i, 0);
                    else                define($i, ($asBitwise === true) ? 1 << ($count - 1) : $count);
                endif;
                $count++;
            endforeach;

    }

    static function setFlashMsg($msg, $color="black")
    {
		$msg = '<span class="color-' . $color . '">' . $msg . '</span>';
        if (!isset($_SESSION['flash_message']))
            $_SESSION['flash_message'] = $msg;
        else
			$_SESSION['flash_message'] .= '<br>' . $msg;
    }

    static function getFlashMsg()
    {
		$msg='';
		if (isset($_SESSION['flash_message']))
		{
			$msg = $_SESSION['flash_message'];
			unset($_SESSION['flash_message']);
		}
        return $msg;
    }

    static function startsWith($haystack, $needle)
	{
		return $needle === "" || strpos($haystack, $needle) === 0;
	}

	static function endsWith($haystack, $needle)
	{
		return $needle === "" || substr($haystack, -strlen($needle)) === $needle;
	}
}
