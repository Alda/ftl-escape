<?php
$smarty->assign('can_act',Helper::canAct($player));

$fleet = $player->getFleet();
$characters = $fleet->getCharacters();
$smarty->assign('characters',$characters);

// we parse ships because we want to avoid multiple objects parsing
$ships = $fleet->getShips();
$shipsArray = array();
foreach ($ships as $ship)
{
    array_push($shipsArray,array('id'=>$ship->getId(),'name'=>$ship->getName(),'type'=>$i18n->getText($ship->getType()->getName())));
}
$smarty->assign('ships',$shipsArray);
// i18n

$smarty->assign('i18n',$i18n);
$smarty->assign('lbl_characters',$i18n->getText('lbl.characters'));
$smarty->assign('lbl_type',$i18n->getText('lbl.type'));
$smarty->assign('lbl_ship',$i18n->getText('lbl.ship'));
$smarty->assign('lbl_politics',$i18n->getText('lbl.political.orientation'));
$smarty->assign('lbl_transfer',$i18n->getText('lbl.transfer'));
$smarty->assign('lbl_character_attack',$i18n->getText('lbl.character.attack'));
$smarty->assign('lbl_character_defense',$i18n->getText('lbl.character.defense'));
$smarty->assign('lbl_character_moral',$i18n->getText('lbl.character.moral'));
$smarty->assign('lbl_character_popularity',$i18n->getText('lbl.character.popularity'));

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);