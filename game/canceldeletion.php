<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_POST['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$i18n = new I18n();
	$i18n->autoSetLang();

	$player->abortDeletion();
	$entityManager->flush();
	Tools::setFlashMsg($i18n->getText('msg.deletion.canceled'));
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php?page=account');
