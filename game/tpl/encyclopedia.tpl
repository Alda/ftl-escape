<h1>{$enc_lbl_humans}</h1>
<div class="datagrid">
	<table id="tbl_hs">
	<thead>
		<tr><th >{$enc_th_type}</th><th>{$enc_th_hp}</th><th>{$enc_th_attack}</th><th>{$enc_th_defense}</th><th>{$enc_th_passengers}</th><th>{$enc_th_staff}</th><!--<th colspan=6>Productions</th><th colspan=4>Spécial</th><th rowspan=2>Prix</th></tr>
		<tr>--><th class="enc_th_prod">{$enc_th_food}</th><th class="enc_th_prod">{$enc_th_fuel}</th><th class="enc_th_prod">{$enc_th_medicine}</th><th class="enc_th_prod">{$enc_th_materials}</th><th class="enc_th_prod">{$enc_th_moral}</th><th class="enc_th_prod">{$enc_th_staff}</th><th class="enc_th_special">{$lbl_power_mine}</th><th class="enc_th_special">{$lbl_power_repair}</th><th class="enc_th_special">{$lbl_power_scout}</th><th class="enc_th_special">{$lbl_power_produce}</th><th>{$enc_th_price}</th></tr>
	</thead>
	<tbody>
		{foreach $ships as $ship}
		<tr><td><a class="tooltip">{$i18n->getText($ship->getName())}<span class="classic">{$i18n->getText($ship->getDescription())}</span></a></td><td>{$ship->getMaxHP()}</td><td>{$ship->getAttack()}</td><td>{$ship->getDefense()}</td><td>{$ship->getMaxPassengers()}</td><td>{$ship->getQualifiedStaff()}</td><td>{$ship->getFoodProduction()}</td><td>{$ship->getFuelProduction()}</td><td>{$ship->getMedicineProduction()}</td><td>{$ship->getMaterialProduction()}</td><td>{$ship->getMoralProduction()}</td><td>{$ship->getQualifiedStaffPerCycle()}</td>
			<td class="enc_{if $ship->canMine()}green{else}red{/if}"></td>
			<td class="enc_{if $ship->canRepair()}green{else}red{/if}"></td>
			<td class="enc_{if $ship->canScout()}green{else}red{/if}"></td>
			<td class="enc_{if $ship->canProduceShips()}green{else}red{/if}"></td>
		<td>{$ship->getFormatedPrice()}</td>
		</tr>
		{/foreach}
	</tbody>
	</table>
</div>
<h1>{$enc_lbl_enemy}</h1>
<div class="datagrid">
	<table id="tbl_es">
		<thead>
			<tr><th>{$enc_th_type}</th><th>{$enc_th_hp}</th><th>{$enc_th_attack}</th><th>{$enc_th_defense}</th><th>{$enc_th_difficulty}</th></tr>
		</thead>
		<tbody>
		{foreach $ennemy_ships as $ship}
		<tr><td><a class="tooltip">{$i18n->getText($ship->getName())}<span class="classic">{$i18n->getText($ship->getDescription())}</span></a></td><td>{$ship->getMaxHP()}</td><td>{$ship->getAttack()}</td><td>{$ship->getDefense()}</td><td>{$difficulties[$ship->getDifficulty()]}</td></tr>
		{/foreach}
		</tbody>
	</table>
</div>
<h1>{$enc_lbl_history}</h1>
<div class="enc_history">
{$enc_txt_history}
</div>
<h1>{$enc_lbl_concepts}</h1>
<div class="enc_definitions">
	<dl>
		{foreach $definitions as $definition}
		<dt>{if !is_null($definition['icon'])}<img src="img/{$definition['icon']}.png" alt=""/>{/if} {$definition['title']}</dt><dd>{$definition['definition']}</dd>
		{/foreach}
	</dl>
</div>
<script type="text/javascript">
sortable_table("tbl_hs",[12,13,14,15]);
sortable_table("tbl_es",[4]);
</script>
<!-- {$time} -->