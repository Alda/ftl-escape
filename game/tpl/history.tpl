<h1>{$lbl_lost_ships}</h1>
<div class="datagrid">
	<table>
		<thead><tr><th>{$hist_th_name}</th><th>{$hist_th_passengers}</th><th>{$hist_th_killedby}</th><th>{$hist_th_date}</th></tr></thead>
		<tbody>
		{$nblost=0}
		{foreach $history as $item}
		{$nblost = $nblost + $item->getPassengers() + $item->getStaff()}
			<tr><td>{$item->getName()} ({$i18n->getText($item->getType()->getName())})</td><td>{$item->getPassengers() + $item->getStaff()} ({$item->getStaff()})</td><td>{if !is_null($item->getKiller())}{$i18n->getText($item->getKiller()->getName())}{else}N/A{/if}</td><td>{$item->getTime()|date_format:"%d/%m/%y %H:%M:%S"}</td></tr>
		{/foreach}
		<tfoot><tr><td></td><td>{$nblost}</td><td></td><td></td></tr></tfoot>
		</tbody>
		</table>
</div>
