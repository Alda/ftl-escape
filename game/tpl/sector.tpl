<h1>{$lbl_menu_sector}</h1>
{$lbl_materials} : {$materials}<br />
{$lbl_wrecks} : {$wrecks} {if $can_act && $can_scout && $wrecks > 0}<a href="explorewreck.php?token={$token}">{$lbl_scout}</a>{/if}<br />

{if $objective == 1 || $objective == 2}
{$lbl_searched} : {$search_percent}%
{/if}

{if $objective == 1 && !$is_habitable && $search_percent < 100}
	<a href="searchsector.php?token={$token}">{$lbl_search_habitable}</a>
{elseif $objective == 2 && !$has_way_to_earth && $search_percent < 100}
	<a href="searchsector.php?token={$token}">{$lbl_search_earth}</a>
{elseif $objective == 0}
	<a href="index.php?page=politics">{$lbl_choose_objective}</a>
{/if}

{if $is_habitable}
	{$msg_sector_habitable} <a href="settle.php?token={$token}">{$lbl_settle}</a>
{/if}

{if $has_way_to_earth}
	{$msg_found_way_to_earth} <a href="jumptoearth.php?token={$token}">{$lbl_jump_to_earth}</a>
{/if}
