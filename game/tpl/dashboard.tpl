<h1>Radar</h1>
<div class="datagrid">
<table id="tbl_hs">
	<thead><tr><th>{$lbl_ship_name}</th><th>{$lbl_ship_type_name}</th><th>{$lbl_ship_hp}</th></tr></thead>
	<tbody>
		{$macro_warning = $jump_status_num < $safe_jump_num}
{foreach $ships as $ship}
{$enoughStaff = $ship['staff'] != 0}
{$FTLDrive = $ship['FTL']}
{$surviveSystems = $ship['surviveSystems']}
{$oxygen = $ship['oxygenLevel']}
{$alert = !$ship['canJump'] || !$FTLDrive || !$surviveSystems}
{$macro_warning = $macro_warning || !$ship['canJump'] || !$FTLDrive}
<tr {if $alert}class="alert"{/if}>
	<td>{$ship['name']}
		{if $alert}
			{if !$enoughStaff}<a class="tooltip alert_staff"><img src="img/alert_staff.png" /><span class="classic">{$msg_not_enough_staff}</span></a>{/if}
			{if !$FTLDrive}<a class="tooltip alert_ftl"><img src="img/alert_ftl.png" /><span class="classic">{$lbl_alert_ftl}</span></a>{/if}
			{if !$surviveSystems}<a class="tooltip alert_survive"><img src="img/alert_survive.png" /><span class="classic">{$lbl_oxygen_level} : {$oxygen}%</span></a>{/if}
		{/if}
	</td>
	<td>{$ship['type']} (Mk. {$ship['level'] + 1})</td>
	<td {if $ship['hp'] < $ship['maxhp']}class="important"{/if}>{$ship['hp']}/{$ship['maxhp']}</td>
</tr>
{/foreach}
	</tbody>
</table>
</div>

{if $sector_nb_ennemies > 0}
<div class="datagrid">
<form action="attack.php" method="post">
	<input type="hidden" name="token" value="{$token}"/>
<table id="tbl_es">
	<thead><tr><th>{$lbl_ship_name}</th><th>{$lbl_ship_type_name}</th><th>{$lbl_ship_hp}</th><th {if $sector_nb_ennemies > 1}colspan=2{/if}>{$lbl_attack_type}</th></tr></thead>
	<tfoot><tr><td></td><td></td><td></td><td {if $sector_nb_ennemies > 1}colspan=2{/if}><input type="submit" value="{$lbl_attack}"/></td></tr></tfoot>
	<tbody>
{foreach $ennemies as $ennemy name="ennemies"}
<tr><td>{$ennemy['name']}</td><td>{$ennemy['type']}</td><td>{$ennemy['hp']}/{$ennemy['maxhp']}</td><td><input type="radio" name="ennemy" value="{$ennemy['id']}" id="radio{$ennemy['id']}"/><label for="radio{$ennemy['id']}">{$lbl_concentrate_fire} ({$combined_attack_value})</label></td>{if $smarty.foreach.ennemies.first && $sector_nb_ennemies > 1}<td rowspan="{$sector_nb_ennemies}" class="nohover"><input type="radio" name="ennemy" id="radiodf" value="-1" /><label for="radiodf">{$lbl_distributed_fire} ({$distributed_attack_value})</label></td>{/if}</tr>
{/foreach}
</tbody>
</table>
</form>
</div>
{/if}

<h1>{$lbl_ftljump}</h1>
{if $sector_nb_ennemies == 0}
	<a href="preparejump.php?token={$token}">{$lbl_prepare_jump}</a>
	{if $jump_status_num}
		/ <a href="canceljumppreparation.php?token={$token}">{$lbl_cancel_jump}</a>
	{/if}
{/if}

{if $jump_status_num != $FTLbroken && $enough_fuel}
<div class="center">
<table id="jump_level_dashboard" class="jump_level">
	<caption><p id="jump_level_title">{$lbl_jump_status}</p><p id="jump_level_status_text">{$jump_status}</p></caption>
<tr><td class="{if $jump_status_num > 10}jl_green{elseif $jump_status_num == 10}jl_current{else}jl_red{/if}">{$jump_levels[10]}</td></tr>
<tr><td class="{if $jump_status_num > 9}jl_green{elseif $jump_status_num <= 9 && $jump_status_num >= 7}jl_current{else}jl_red{/if}">
	{$jump_status_sync}
	{if $jump_status_num <= 9 && $jump_status_num >= 7}
	<div class="mini_load">
		{if $jump_status_num >= 7}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
		{if $jump_status_num >= 7}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
		{if $jump_status_num >= 8}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
		{if $jump_status_num >= 9}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
	</div>
	{/if}
	</td></tr>
<tr><td class="{if $jump_status_num > 6}jl_green{elseif $jump_status_num <= 6 && $jump_status_num >= 3}jl_current{else}jl_red{/if}">
	{$jump_status_warming}
	{if $jump_status_num <= 6 && $jump_status_num >= 3}
	<div class="mini_load">
		{if $jump_status_num >= 3}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
		{if $jump_status_num >= 4}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
		{if $jump_status_num >= 5}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
		{if $jump_status_num >= 6}<img src="img/mini_load_ok.png" alt="ok"/>{else}<img src="img/mini_load_ko.png" alt="ko"/>{/if}
	</div>
	{/if}
	</td></tr>
<tr><td class="{if $jump_status_num > 2}jl_green{elseif $jump_status_num == 2}jl_current{else}jl_red{/if}">{$jump_levels[2]}</td></tr>
<tr><td class="{if $jump_status_num > 1}jl_green{elseif $jump_status_num == 1}jl_current{else}jl_red{/if}">{$jump_levels[1]}</td></tr>
<tr><td class="{if $jump_status_num > 0}jl_green{elseif $jump_status_num == 0}jl_current{else}jl_red{/if}">{$jump_levels[0]}</td></tr>
</table>

{if $jump_status_num >= $min_jumpable_status}
<form id="jumpform" name="jumpform" action="jump.php" method="post">
	<input type="hidden" name="token" value="{$token}"/>
{/if}
<input
   type="submit"
   name="jumpbtn"
   class="jumpbutton{if $jump_status_num < $min_jumpable_status}disabled{/if}"
   value="{$lbl_jump}"
   onMouseOver="goLite(this.form.name,this.name)"
   onMouseOut="goDim(this.form.name,this.name)"
   {if $macro_warning}onClick="return overlay();"{/if}>
{if $jump_status_num >= $min_jumpable_status}
</form>
{/if}
</div>
{elseif !$enough_fuel}
{$msg_not_enough_fuel}
{else}
{$jump_levels[-1]}
{/if}

<script type="text/javascript">
sortable_table("tbl_hs",[2]);
{if $sector_nb_ennemies > 0}
sortable_table("tbl_es");
{/if}
</script>

<div id="overlay">
	<div id="jump_alert">
		<h1>{$lbl_jump_alert}</h1>
		<p>{$msg_alert}</p>
		<p><a onclick="return confirmJump()">{$lbl_confirm}</a> <a onclick="return overlay()">{$lbl_cancel}</a></p>
	</div>
</div>

<script type="text/javascript">
function overlay() {
    el = document.getElementById("overlay");
   el.style.visibility = (el.style.visibility == "visible") ? "hidden" : "visible";
   return false;
 }
function confirmJump()
{
	document.getElementById("jumpform").submit();
}
</script>
