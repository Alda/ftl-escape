<nav id="menu-nav">
	<ul>
		<li><a href="?">{$menu_dashboard}{if $under_attack} (!){/if}</a></li>
		<li><a href="?page=fleet">{$menu_fleet}</a></li>
		<li><a href="?page=sector">{$menu_sector}{if $alert_sector} (!){/if}</a></li>
		<!--<li><a href="?page=event">{$menu_event}</a></li>-->
		<li><a href="?page=politics">{$menu_politics}</a></li>
		<li><a href="?page=characters">{$menu_characters}</a></li>
		<li><a href="?page=history">{$menu_history}</a></li>
		<li><a href="?page=messages">{$menu_messages}{if $nb_messages > 0} ({$nb_messages}){/if}</a></li>
		<li><a href="?page=galaxy">{$menu_galaxy}</a></li>
		<li><a href="?page=account">{$menu_account}{if $account_deletion} (!){/if}</a></li>
		<li><a href="?page=encyclopedia">{$menu_encyclopedia}</a></li>
		<li><a href="logout.php">{$menu_signout}</a></li>
	</ul>
</nav>
