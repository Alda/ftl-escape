<h1>{$lbl_planets}</h1>
<ul id="gplanets">
{foreach $planets as $planet}
<li class="pstatus{$planet->getStatus()}">
	<div class="planet">
		<span class="title">{$planet->getName()}</span><br />
		{if $planet->getStatus() == 0}
			{$lbl_freed_by} : {$planet->getFreedBy()->getName()}<br />
		{/if}
		{if $planet->getStatus() == 2}
			{$lbl_attacked_by} : {$planet->getSector()->getPlayer()->getName()}<br />
		{/if}
	</div>
</li>
{/foreach}
</ul>
<div style="clear:both;"></div>
<h1>{$lbl_galaxy}</h1>
<div class="datagrid">
<table id="tbl_galaxy">
<thead><tr><th>{$lbl_player}</th><th>{$lbl_nb_ships}</th><th>{$lbl_nb_survivors}</th></tr></thead>
<tbody>
	{$nb_ships=0}
	{$nb_survivors=0}
	{foreach $players as $player}
	{$nbs = count($player->getFleet()->getShips())}
	{$nbv = Helper::calculateSurvivors($player)}
	{$nb_ships = $nb_ships + $nbs}
	{$nb_survivors = $nb_survivors + $nbv}
	<tr><td>{$player->getLogin()}</td><td>{$nbs}</td><td>{$nbv}</td></tr>
	{/foreach}
</tbody>
<tfoot><tr><td></td><td>{$nb_ships}</td><td>{$nb_survivors}</td></tr></tfoot>
</table>
</div>
<script type="text/javascript">
sortable_table("tbl_galaxy");
</script>
