<?php
$fleet = $player->getFleet();

//echo $fleet->getCombinedAttack();

$smarty->assign('fleet_name',$fleet->getName());

$ships = $fleet->getShips();
$shipsArr = array();
$survivors = 0;
$everyShipCanJump = true;
foreach ($ships as $ship)
{
	$level = $ship->getLevel();
	$shipArr=array('name'=>$ship->getName(),'hp'=>$ship->getHP(),'maxhp'=>$ship->getType()->getMaxHP($level),'type'=>$i18n->getText($ship->getType()->getName()),'level'=>$ship->getLevel(),'staff'=>$ship->getStaff(),'canJump'=>$ship->canJump(),'FTL'=>$ship->getFTLDrive(),'surviveSystems'=>$ship->getSurviveSystems(),'oxygenLevel'=>$ship->getOxygenLevel());
	array_push($shipsArr,$shipArr);
	$survivors += $ship->getPassengers() + $ship->getStaff();
}

$smarty->assign('ships',$shipsArr);

$sector = $player->getSector();

$smarty->assign('sector_material',$sector->getMaterial());

$smarty->assign('jump_status_num',$fleet->getJumpStatus());
$smarty->assign('min_jumpable_status',MIN_JUMP_STATUS_FOR_JUMP);

$ennemies = $sector->getEnnemies();

$nbennemies = count($ennemies);
$smarty->assign('sector_nb_ennemies',$nbennemies);
if ($nbennemies > 0)
{
	$smarty->assign('combined_attack_value',$fleet->getCombinedAttack());
	$smarty->assign('distributed_attack_value',round($fleet->getCombinedAttack()/count($ennemies),0));
}
$ennemiesArr = array();

foreach ($ennemies as $ennemy)
{
	$ennemyArr = array('id'=>$ennemy->getId(),'name'=>$ennemy->getName(),'hp'=>$ennemy->getHP(),'maxhp'=>$ennemy->getType()->getMaxHP(),'type'=>$i18n->getText($ennemy->getType()->getName()));
	array_push($ennemiesArr,$ennemyArr);
}

$smarty->assign('ennemies',$ennemiesArr);

$smarty->assign('enough_fuel',$fleet->getFuel() >= count($ships));
$smarty->assign('safe_jump_num',JUMP_STATUS_SAFE);

// I18N

$smarty->assign('lbl_concentrate_fire',$i18n->getText('lbl.concentrate.fire'));
$smarty->assign('lbl_distributed_fire',$i18n->getText('lbl.distributed.fire'));
$smarty->assign('lbl_attack',$i18n->getText('lbl.attack'));
$smarty->assign('lbl_jump_status',$i18n->getText('lbl.jump.status'));
$smarty->assign('lbl_jump',$i18n->getText('lbl.jump'));
$smarty->assign('lbl_prepare_jump',$i18n->getText('lbl.prepare.jump'));
$smarty->assign('lbl_prepare_jump',$i18n->getText('lbl.prepare.jump'));
$smarty->assign('lbl_ftljump',$i18n->getText('lbl.ftljump'));

$smarty->assign('lbl_oxygen_level',$i18n->getText('lbl.oxygen.level'));
$smarty->assign('lbl_alert_ftl',$i18n->getText('lbl.alert.ftl'));

$smarty->assign('lbl_ship_name',$i18n->getText('lbl.ship.name'));
$smarty->assign('lbl_ship_type_name',$i18n->getText('lbl.ship.type.name'));
$smarty->assign('lbl_ship_hp',$i18n->getText('lbl.ship.hp'));
$smarty->assign('lbl_attack_type',$i18n->getText('lbl.attack.type'));

$smarty->assign('jump_status',$i18n->getText('jump.status.'.$fleet->getJumpStatus()));

$smarty->assign('min_jump_level',MIN_JUMP_STATUS);
$smarty->assign('max_jump_level',MAX_JUMP_STATUS);
$smarty->assign('FTLbroken',JUMP_STATUS_OUT_OF_ORDER);
$smarty->assign('jump_status_warming',$i18n->getText('jump.status.warming.up'));
$smarty->assign('jump_status_sync',$i18n->getText('jump.status.sync'));
$jumplevelsArr = array();
for ($i=MIN_JUMP_STATUS;$i<=MAX_JUMP_STATUS;$i++)
{
	$jumplevelsArr[$i]=$i18n->getText('jump.status.'.$i);
}
$smarty->assign('jump_levels',$jumplevelsArr);

$smarty->assign('msg_not_enough_fuel',$i18n->getText('msg.not.enough.fuel'));
$smarty->assign('msg_not_enough_staff',$i18n->getText('msg.not.enough.staff'));
$smarty->assign('lbl_jump_alert',$i18n->getText('lbl.jump.alert'));
$smarty->assign('msg_alert',$i18n->getText('msg.jump.will.lost.ships'));
$smarty->assign('lbl_confirm',$i18n->getText('lbl.confirm'));
$smarty->assign('lbl_cancel',$i18n->getText('lbl.cancel'));
$smarty->assign('lbl_cancel_jump',$i18n->getText('lbl.cancel.jump'));

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);
