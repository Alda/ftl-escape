<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once __DIR__.'/../bootstrap.php';
require_once __DIR__.'/../const.php';
require_once __DIR__.'/../builder.php';
require_once __DIR__.'/../helper.php';

if (Helper::checkCSRF($_GET['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	if ($player->isGameOver())
	{
		Builder::restart($player);
		
		$entityManager->flush();
	}
	else
	{
		echo "It's not finished for you yet.";
	}
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php');
