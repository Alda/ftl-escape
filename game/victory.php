<?php

$objective = $player->getObjectiveType();

$victory_txt='';
if ($objective == OBJECTIVE_SEARCH_HABITABLE_ID)
{
	$victory_txt = 'txt.victory.habitable';
}
elseif ($objective == OBJECTIVE_SEARCH_EARTH_ID)
{
	$victory_txt = 'txt.victory.earth';
}
else
{
	$victory_txt = 'nope';
}

$smarty->assign('lbl_victory',$i18n->getText('lbl.victory'));
$smarty->assign('victory_txt',$i18n->getText($victory_txt));

$smarty->assign('fleet',$player->getFleet());

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);
