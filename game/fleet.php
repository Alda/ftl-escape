<?php
$fleet = $player->getFleet();
$sector = $player->getSector();
$canAct = count($sector->getEnnemies()) == 0;

$smarty->assign('can_act',$canAct);
$smarty->assign('fleetname',$fleet->getName());
$smarty->assign('fleet',$fleet);

$ships = $fleet->getShips();

$smarty->assign('ships',$ships);
$smarty->assign('nb_ships',count($ships));

$survivors = 0;
$foodproduction = 0;
$fuelproduction = 0;
$materialproduction = 0;
$moralproduction = 0;
$medicineproduction = 0;
$staffproduction = 0;
$canRepair=false;
$canScout=false;
$canMine=false;
$canProduce=false;
$defense=0;

foreach ($ships as $ship)
{
	$level = $ship->getLevel();
	$efficiency = $ship->getEfficiency();
	$survivors += $ship->getPassengers() + $ship->getStaff();
	$foodproduction += round($ship->getType()->getFoodProduction($level) * $efficiency);
	$fuelproduction += round($ship->getType()->getFuelProduction($level) * $efficiency);
	$materialproduction += round($ship->getType()->getMaterialProduction($level) * $efficiency);
	$moralproduction += $ship->getType()->getMoralProduction();
	$medicineproduction += round($ship->getType()->getMedicineProduction($level) * $efficiency);
	$staffproduction += round($ship->getType()->getQualifiedStaffPerCycle($level) * $efficiency);
	$canRepair = $canRepair || $ship->getType()->canRepair();
	$canScout = $canScout || $ship->getType()->canScout();
	$canMine = $canMine || $ship->getType()->canMine();
	$canProduce = $canProduce || $ship->getType()->canProduceShips();
	$defense += $ship->getDefense($level);
}

$productionBonus=1;
$moralBonus=1;
$attackBonus; // already took into account in Ship->getAttack() and Fleet->getCombinedAttack()
$defenseBonus; // already took into account in Ship->getDefense() and Fleet->getCombinedDefense()

$politicalsystem = $fleet->getPoliticalSystem();
if (!is_null($politicalsystem))
{
	$productionBonus = 1 + $politicalsystem->getProductionBonus();
	$moralBonus = 1 + $politicalsystem->getMoralBonus();
	$attackBonus = 1 + $politicalsystem->getAttackBonus();
	
	$foodproduction = $foodproduction * $productionBonus;
	$fuelproduction = $fuelproduction * $productionBonus;
	$foodproduction = $foodproduction * $productionBonus;
	$materialproduction = $materialproduction * $productionBonus;
	$medicineproduction = $medicineproduction * $productionBonus;
}

$characters = $fleet->getCharacters();
foreach ($characters as $character)
{
	$moralBonus += $character->getMoralBonus();
}

$smarty->assign('survivors',$survivors);
$smarty->assign('food_production',round($foodproduction));
$smarty->assign('food_conso',round($survivors/FOOD_CONSUMPTION));
$smarty->assign('medicine_conso',round($survivors/MEDICINE_CONSUMPTION));
$smarty->assign('fuel_production',round($fuelproduction));
$smarty->assign('fuel_conso',round(count($ships)/NB_SHIPS_FOR_FUEL_CONSUMPTION));
$smarty->assign('material_production',round($materialproduction));
$smarty->assign('moral_production',round($moralproduction * $moralBonus));
$smarty->assign('medicine_production',round($medicineproduction));
$smarty->assign('staff_production',round($staffproduction));
$smarty->assign('combined_attack',$fleet->getCombinedAttack());
$smarty->assign('combined_defense',$defense);
$smarty->assign('can_repair',$canRepair);
$smarty->assign('can_scout',$canScout);
$smarty->assign('can_mine',$canMine);
$smarty->assign('can_produce',$canProduce);
$smarty->assign('has_admiral_ship',!is_null($fleet->getAdmiralShip()));
$smarty->assign('admiral_ship',$fleet->getAdmiralShip());
$smarty->assign('admiral_defense_bonus',ADMIRAL_SHIP_DEFENSE_BONUS);
$smarty->assign('nb_ships',count($ships));
$smarty->assign('ship_level_multiplier',SHIP_LEVEL_MULTIPLIER);
$smarty->assign('upgrade_ratio',SHIP_UPGRADE_PRICE);
$smarty->assign('max_upgrade_level',SHIP_UPGRADE_MAX_LEVEL);

if ($canAct && $canProduce)
{
	$qb = $entityManager->createQueryBuilder();
	$qb->select('s')
		->from('ShipType','s')
		->where('s.price <= ?1')
		->andWhere('s.buildable = ?2')
		->setParameter(1,$fleet->getMaterial())
		->setParameter(2,true);
	$query = $qb->getQuery();
	$buildableShips = $query->getResult();
	$smarty->assign('available_production',$buildableShips);
}

// i18n

$smarty->assign('i18n',$i18n);
if (count($ships) > 1)
{
	$smarty->assign('w_ships',$i18n->getText('word.ships'));
}
else
{
	$smarty->assign('w_ships',$i18n->getText('word.ship'));
}
$smarty->assign('lbl_repair',$i18n->getText('lbl.repair'));
$smarty->assign('lbl_ship_name',$i18n->getText('lbl.ship.name'));
$smarty->assign('lbl_ship_type_name',$i18n->getText('lbl.ship.type.name'));
$smarty->assign('lbl_ship_hp',$i18n->getText('lbl.ship.hp'));
$smarty->assign('lbl_ship_attack',$i18n->getText('lbl.ship.attack'));
$smarty->assign('lbl_ship_defense',$i18n->getText('lbl.ship.defense'));
$smarty->assign('lbl_ship_passengers',$i18n->getText('lbl.ship.passengers'));
$smarty->assign('lbl_ship_staff',$i18n->getText('lbl.ship.staff'));
$smarty->assign('lbl_ship_fuel_production',$i18n->getText('lbl.ship.fuel.production'));
$smarty->assign('lbl_ship_food_production',$i18n->getText('lbl.ship.food.production'));
$smarty->assign('lbl_ship_medicine_production',$i18n->getText('lbl.ship.medicine.production'));
$smarty->assign('lbl_ship_moral_production',$i18n->getText('lbl.ship.moral.production'));
$smarty->assign('lbl_ship_material_production',$i18n->getText('lbl.ship.material.production'));
$smarty->assign('lbl_power_repair',$i18n->getText('lbl.power.repair'));
$smarty->assign('lbl_power_mine',$i18n->getText('lbl.power.mine'));
$smarty->assign('lbl_power_scout',$i18n->getText('lbl.power.scout'));
$smarty->assign('lbl_power_produce',$i18n->getText('lbl.power.produce'));
$smarty->assign('lbl_transfer',$i18n->getText('lbl.transfer'));
$smarty->assign('lbl_build',$i18n->getText('lbl.build'));
$smarty->assign('fleet_th_stock',$i18n->getText('fleet.th.stock'));
$smarty->assign('fleet_th_conso',$i18n->getText('fleet.th.conso'));
$smarty->assign('fleet_th_prod',$i18n->getText('fleet.th.prod'));
$smarty->assign('msg_not_enough_staff',$i18n->getText('msg.not.enough.staff'));
$smarty->assign('lbl_protect_fleet',$i18n->getText('lbl.protect.fleet'));
$smarty->assign('lbl_protect_ship',$i18n->getText('lbl.protect.ship'));
$smarty->assign('lbl_unprotect_ship',$i18n->getText('lbl.unprotect.ship'));
$smarty->assign('lbl_oxygen_level',$i18n->getText('lbl.oxygen.level'));
$smarty->assign('lbl_alert_ftl',$i18n->getText('lbl.alert.ftl'));
$smarty->assign('lbl_recycle_ship',$i18n->getText('lbl.recycle.ship'));
$smarty->assign('msg_ftl_active_only',$i18n->getText('msg.ftl.active.only'));

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);
