<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_GET['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$objective = $player->getObjectiveType();

	$sector = $player->getSector();

	$i18n = new I18n();
	$i18n->autoSetLang();

	if (Helper::canAct($player))
	{
		if ($sector->getSearched() < 100)
		{
			if ($objective == OBJECTIVE_SEARCH_HABITABLE_ID)
			{
				$fleet = $player->getFleet();
				if (Helper::canScout($fleet))
				{
					$ships = $fleet->getShips();
					$nbscout = 0;
					foreach ($ships as $ship)
					{
						if ($ship->getType()->canScout())
						{
							$nbscout++;
							$dice = rand(1,10000);
							if ($dice <= CHANCE_TO_FIND_HABITABLE)
							{
								$sector->setHabitable();
							}
						}
					}
					if ($sector->isHabitable())
					{
						Tools::setFlashMsg($i18n->getText('msg.found.habitable.planet'));
					}
					$sector->increaseSearch($nbscout);
				}
				else
				{
					Tools::setFlashMsg($i18n->getText('msg.cannot.scout'));
				}
			}
			elseif ($objective == OBJECTIVE_SEARCH_EARTH_ID)
			{
				$fleet = $player->getFleet();
				if (Helper::canScout($fleet))
				{
					$ships = $fleet->getShips();
					$nbscout = 0;
					foreach ($ships as $ship)
					{
						if ($ship->getType()->canScout())
						{
							$nbscout++;
							$dice = rand(1,10000);
							if ($dice <= CHANCE_TO_FIND_EARTH_CLUE)
							{
								Tools::setFlashMsg($i18n->getText('msg.found.earth.clue'));
								$player->findEarthClue();
							}
						}
					}
					$sector->increaseSearch($nbscout);
				}
				else
				{
					Tools::setFlashMsg($i18n->getText('msg.cannot.scout'));
				}
			}
		}
		else
		{
			Tools::setFlashMsg($i18n->getText('msg.sector.already.searched'));
		}
	}
	$entityManager->flush();
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php?page=sector');
