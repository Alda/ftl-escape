<?php
include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../builder.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_POST['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$i18n = new I18n();
	$i18n->autoSetLang();
	if (Helper::hasEvent($player))
	{
		Tools::setFlashMsg($i18n->getText('msg.cannot.jump.while.event'));
	}
	else
	{
		$fleet = $player->getFleet();
		$jumpstatus = $fleet->getJumpStatus();
		$admiral = $fleet->getAdmiralShip();
		if (!is_null($admiral) && $admiral->getStaff() == 0)
		{
			Tools::setFlashMsg($i18n->getText('msg.admiral.ship.unable.to.jump'));
		}
		else
		{
			$nbshiplost = 0;
			$ships = $fleet->getShips();
			$ids = array();
			foreach ($ships as $ship)
			{
				if (is_null($admiral) || $fleet->getAdmiralShip()->getId() != $ship->getId())
				{
					array_push($ids,$ship->getId());
				}
			}

			if ($jumpstatus == MIN_JUMP_STATUS_FOR_JUMP)
			{
				$nbshiplost = round(count($ids) * 0.75,0);
			}
			elseif ($jumpstatus == JUMP_STATUS_SAVE_50_PERCENT)
			{
				$nbshiplost = round(count($ids) * 0.5,0);
			}
			elseif ($jumpstatus == JUMP_STATUS_SAVE_75_PERCENT)
			{
				$nbshiplost = round(count($ids) * 0.25,0);
			}
			elseif ($jumpstatus >= JUMP_STATUS_SAFE)
			{
				$nbshiplost = 0;
			}
			else
			{
				throw new Exception("Not allowed to jump");
			}

			$fleet->decreaseFuel(count($ships));
			$fleet->decreaseMoral($nbshiplost);

			Tools::setFlashMsg($i18n->getText('msg.jump.complete'));

			if ($nbshiplost > 0)
			{
				Tools::setFlashMsg($i18n->getText('msg.jump.lost.ships'));
				$lostships = array();
				for ($i=0;$i<$nbshiplost;$i++)
				{
					$lostShip = $fleet->getShip($ids[rand(0,count($ids)-1)]);
					array_push($lostships,array('id'=>$lostShip->getId(),'name'=>$lostShip->getName(),'type'=>$lostShip->getType()->getName()));
					$fleet->removeShip($lostShip);
					Helper::destroyShip($lostShip);
					$entityManager->remove($lostShip);
					$ids = array();
					foreach ($ships as $ship)
					{
						array_push($ids,$ship->getId());
					}
				}
				foreach ($lostships as $lostship)
				{
					Tools::setFlashMsg($i18n->getText('msg.jump.lost.ship',array($lostship['name'],$i18n->getText($lostship['type']))));
				}
			}
			else
			{
				Tools::setFlashMsg($i18n->getText('msg.jump.no.lost.ships'));
			}

			// we check if the ships can jump or not
			$ships = $fleet->getShips();
			foreach ($ships as $ship)
			{
				if ($ship->getStaff() == 0 || !$ship->getFTLDrive())
				{
					Tools::setFlashMsg($i18n->getText('msg.ship.unable.to.jump',array($ship->getName(),$i18n->getText($ship->getType()->getName()))));
					Helper::destroyShip($ship);
					$fleet->removeShip($ship);
					$entityManager->remove($ship);
				}
			}

			$fleet->setJumpStatus(JUMP_STATUS_IDLE);
			$sector=$player->getSector();
			Builder::freeSector($sector);
			$sector = Builder::buildSector($player);

			$entityManager->persist($sector);

			$entityManager->flush();
		}
	}
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php');
