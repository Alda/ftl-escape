<?php

$smarty->assign('login',$player->getLogin());
$smarty->assign('email',$player->getEmail());
$deletiondate = $player->getDeletionDate();
$smarty->assign('account_in_deletion',!is_null($deletiondate));
if (!is_null($deletiondate))
{
	$smarty->assign('deletion_timestamp',($deletiondate + DELETION_PERIOD));
	$smarty->assign('deletion_date',date('d/m/Y H:i e',($deletiondate + DELETION_PERIOD)));
}
$difficulty = Helper::getDifficulty($player);
$smarty->assign('difficulty_txt',$i18n->getText('lbl.difficulty.'.$difficulty));
$smarty->assign('calculated_difficulty',is_null($player->getDifficulty()));
$smarty->assign('difficulty',$difficulty);

$smarty->assign('badges',$player->getBadges());

$smarty->assign('max_difficulty',MAX_DIFFICULTY);

// I18n
$smarty->assign('i18n',$i18n);
$smarty->assign('lbl_badges',$i18n->getText('lbl.badges'));
$smarty->assign('lbl_account',$i18n->getText('lbl.account'));
$smarty->assign('lbl_login',$i18n->getText('lbl.login'));
$smarty->assign('lbl_email',$i18n->getText('lbl.email'));
$smarty->assign('lbl_password',$i18n->getText('lbl.password'));
$smarty->assign('lbl_password_retype',$i18n->getText('lbl.password.again'));
$smarty->assign('lbl_save',$i18n->getText('lbl.save'));
$smarty->assign('lbl_delete',$i18n->getText('lbl.delete.account'));
$smarty->assign('lbl_account_in_deletion',$i18n->getText('lbl.account.in.deletion'));
$smarty->assign('lbl_cancel',$i18n->getText('lbl.cancel'));
$smarty->assign('lbl_confirm',$i18n->getText('lbl.confirm'));
$smarty->assign('lbl_current_password',$i18n->getText('lbl.current.password'));
$smarty->assign('lbl_difficulty',$i18n->getText('lbl.difficulty'));
$smarty->assign('lbl_restart',$i18n->getText('lbl.restart'));
$smarty->assign('lbl_restart_alert',$i18n->getText('lbl.restart.alert'));
$smarty->assign('lbl_calculated_difficulty',$i18n->getText('lbl.calculated.difficulty'));
$smarty->assign('lbl_calculated',$i18n->getText('lbl.calculated'));
$smarty->assign('msg_alert',$i18n->getText('msg.restart.alert'));

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);
