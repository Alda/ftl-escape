<?php
$objectiveType = $player->getObjectiveType();
$smarty->assign('objective_type',$objectiveType);
if ($objectiveType == OBJECTIVE_SEARCH_EARTH_ID)
{
	$smarty->assign('nb_clues',$player->getEarthClues());
	$smarty->assign('max_clues',NB_CLUES_TO_EARTH);
}

if ($objectiveType == OBJECTIVE_RETAKE_COLONIES_ID)
{
	$planets = $entityManager->getRepository('Planet')->findAll();
	$smarty->assign('planets',$planets);
}
$fleet = $player->getFleet();
$politicalSystem = $fleet->getPoliticalSystem();
if (empty($politicalSystem))
{
	$smarty->assign('political_systems',$entityManager->getRepository('PoliticalSystem')->findAll());
}
else
{
	$smarty->assign('political_system',$politicalSystem);
}

$eships = $entityManager->getRepository('EnnemyShipType')->findAll();
$shipsArray = array();
foreach ($eships as $ship)
{
	$shipsArray[$ship->getId()] = $ship;
}

$smarty->assign('enemy_ships',$shipsArray);

$smarty->assign('can_act',Helper::canAct($player));
$smarty->assign('can_scout',Helper::canScout($fleet));

$smarty->assign('max_attacks',PLANET_MAX_ATTACK_PER_DAY);

// i18n

$smarty->assign('i18n',$i18n);
$smarty->assign('lbl_search_habitable',$i18n->getText('lbl.objective.search.habitable'));
$smarty->assign('lbl_search_habitable_bonus',$i18n->getText('lbl.objective.search.habitable.bonus'));
$smarty->assign('lbl_search_habitable_difficulty',$i18n->getText('lbl.objective.search.habitable.difficulty'));
$smarty->assign('lbl_search_earth',$i18n->getText('lbl.objective.search.earth'));
$smarty->assign('lbl_search_earth_bonus',$i18n->getText('lbl.objective.search.earth.bonus'));
$smarty->assign('lbl_search_earth_difficulty',$i18n->getText('lbl.objective.search.earth.difficulty'));
$smarty->assign('lbl_objective',$i18n->getText('lbl.objective'));
$smarty->assign('lbl_confirm',$i18n->getText('lbl.confirm'));
$smarty->assign('lbl_earth_relics',$i18n->getText('lbl.earth.relics'));
$smarty->assign('msg_found_way_to_earth',$i18n->getText('msg.found.way.to.earth'));
$smarty->assign('lbl_jump_to_earth',$i18n->getText('lbl.jump.to.earth'));
$smarty->assign('lbl_system_name',$i18n->getText('lbl.system.name'));
$smarty->assign('lbl_system_liberty',$i18n->getText('lbl.system.liberty'));
$smarty->assign('lbl_system_equality',$i18n->getText('lbl.system.equality'));
$smarty->assign('lbl_system_production',$i18n->getText('lbl.system.production'));
$smarty->assign('lbl_system_religion',$i18n->getText('lbl.system.religion'));
$smarty->assign('lbl_system_election',$i18n->getText('lbl.system.election'));
$smarty->assign('lbl_system_moral',$i18n->getText('lbl.system.moral'));
$smarty->assign('lbl_system_defense',$i18n->getText('lbl.system.defense'));
$smarty->assign('lbl_system_attack',$i18n->getText('lbl.system.attack'));
$smarty->assign('lbl_system_corruption',$i18n->getText('lbl.system.corruption'));
$smarty->assign('lbl_system_justice',$i18n->getText('lbl.system.independant.justice'));
$smarty->assign('lbl_system_popcontrol',$i18n->getText('lbl.system.population.control'));
$smarty->assign('lbl_political_system',$i18n->getText('lbl.political.system'));
$smarty->assign('lbl_yes',$i18n->getText('lbl.yes'));
$smarty->assign('lbl_no',$i18n->getText('lbl.no'));
$smarty->assign('lbl_choose',$i18n->getText('lbl.choose'));
$smarty->assign('lbl_attacked_by',$i18n->getText('lbl.attacked.by'));
$smarty->assign('lbl_attack',$i18n->getText('lbl.attack'));
$smarty->assign('lbl_scout',$i18n->getText('lbl.scout'));
$smarty->assign('lbl_freed_by',$i18n->getText('lbl.freed.by'));
$smarty->assign('lbl_known_forces',$i18n->getText('lbl.known.forces'));

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);
