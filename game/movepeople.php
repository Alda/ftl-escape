<?php

include __DIR__.'/../lib/session.inc.php';
check_login();

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../helper.php');
require_once(__DIR__.'/../lib/i18n.php');

if (Helper::checkCSRF($_POST['token']))
{
	$username = $_SESSION['username'];
	$player = $entityManager->getRepository('Player')->findOneByLogin($username);

	$i18n = new I18n();
	$i18n->autoSetLang();

	$fleet = $player->getFleet();

	$sector = $player->getSector();

	$ennemies = $sector->getEnnemies();

	if (count($ennemies) == 0)
	{
		$nbpassengers = $_POST['valnbpassengers'];
		$nbstaff = $_POST['valnbstaff'];
		
		$ships = $fleet->getShips();
		$checknbpassengers = 0;
		$checknbstaff = 0;
		foreach ($ships as $ship)
		{
			$checknbpassengers += $ship->getPassengers();
			$checknbstaff += $ship->getStaff();
		}
		$values = array_values($nbpassengers);
		foreach ($values as $value)
		{
			$checknbpassengers -= $value;
		}
		$values = array_values($nbstaff);
		foreach ($values as $value)
		{
			$checknbstaff -= $value;
		}
		if ($checknbpassengers != 0 || $checknbstaff !=0)
		{
			Tools::setFlashMsg($i18n->getText('msg.impossible.action'));
		}
		else
		{
			foreach ($nbpassengers as $shipid => $nb)
			{
				$ship = $entityManager->getRepository('Ship')->find($shipid);
				if ($ship->getFleet()->getId() == $fleet->getId() && $nb <= $ship->getType()->getMaxPassengers())
				{
					$ship->setPassengers($nb);
				}
				else
				{
					throw new Exception('This ship does not belong to this fleet or the maximum passengers has been exceeded');
				}
			}
			
			foreach ($nbstaff as $shipid => $nb)
			{
				$ship = $entityManager->getRepository('Ship')->find($shipid);
				if ($ship->getFleet()->getId() == $fleet->getId() && $nb <= $ship->getType()->getQualifiedStaff())
				{
					$ship->setStaff($nb);
				}
				else
				{
					throw new Exception('This ship does not belong to this fleet or the maximum staff has been exceeded');
				}
			}
			$entityManager->flush();
		}
	}
}
else
{
	Tools::setFlashMsg('msg.wrong.token');
}
header('Location: index.php?page=fleet');
