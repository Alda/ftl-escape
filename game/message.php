<?php

$messageid=$_GET['id'];

$messageItem = $entityManager->getRepository('Message')->find($messageid);

if (!is_null($messageItem) && ($messageItem->getRecipient()->getId() == $player->getId() || (!is_null($messageItem->getSender()) && $messageItem->getSender()->getId() == $player->getId())))
{
	$smarty->assign('message',$messageItem);
	$smarty->assign('i18n',$i18n);
	$messageItem->read();
	$entityManager->flush();
}
else
{
	Tools::setFlashMsg($i18n->getText('msg.message.not.for.you'));
}
