<?php

$sector = $player->getSector();
$fleet = $player->getFleet();

$canScout = Helper::canScout($fleet);

$smarty->assign('materials',$sector->getMaterial());
$smarty->assign('wrecks',$sector->getWrecks());
$smarty->assign('can_scout',$canScout);
$smarty->assign('can_act',count($sector->getEnnemies())==0);

$smarty->assign('objective',$player->getObjectiveType());
$smarty->assign('is_habitable',$sector->isHabitable());
$smarty->assign('search_percent',$sector->getSearched());
$smarty->assign('has_way_to_earth',$player->getEarthClues() == NB_CLUES_TO_EARTH);

// i18n

$smarty->assign('lbl_materials',$i18n->getText('lbl.materials'));
$smarty->assign('lbl_wrecks',$i18n->getText('lbl.wrecks'));
$smarty->assign('lbl_scout',$i18n->getText('lbl.scout.wreck'));
$smarty->assign('lbl_menu_sector',$i18n->getText('lbl.menu.sector'));
$smarty->assign('lbl_searched',$i18n->getText('lbl.sector.searched'));
$smarty->assign('lbl_search_habitable',$i18n->getText('lbl.search.for.habitable'));
$smarty->assign('lbl_search_earth',$i18n->getText('lbl.search.for.earth.clue'));
$smarty->assign('msg_sector_habitable',$i18n->getText('msg.sector.habitable'));
$smarty->assign('lbl_settle',$i18n->getText('lbl.settle'));
$smarty->assign('lbl_choose_objective',$i18n->getText('lbl.choose.objective'));
$smarty->assign('msg_found_way_to_earth',$i18n->getText('msg.found.way.to.earth'));
$smarty->assign('lbl_jump_to_earth',$i18n->getText('lbl.jump.to.earth'));

// CSRF
$token = Helper::generateCSRFToken();
$smarty->assign('token',$token);
