<?php
/**
 * @Entity @Table(name="planets")
 **/
 
 use Doctrine\Common\Collections\ArrayCollection;
 
class Planet
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text") **/
    protected $name;
    /** @Column(type="array",nullable=true) **/
    protected $garrison=array();
    /** @Column(type="integer", options={"default"=0}) **/
    protected $materials=0;
    /** @Column(type="integer", options={"default"=0}) **/
    protected $materialproduction=1;
    /** @Column(type="boolean") **/
    protected $isattacked=false;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $initialpopulation=0;
    /** @Column(type="integer",options={"default"=1}) **/
    protected $status=1;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $inteldate=0;
    /** @Column(type="array",nullable=true) **/
    protected $knowngarrison=array();
    /** @OneToOne (targetEntity="Sector",inversedBy="planet") **/
    private $sector;
    /** @ManyToOne (targetEntity="Player") **/
    private $freedBy;
    /** @Column(type="integer",options={"default"=0}) **/
    protected $attackcount=0;
    
    /*
     * garrison
     * array(EnemyShipTypeId=>nb)
     */
    
    /*
     *  status
     *  0 : freed
     *  1 : occupied
     *  2 : under allied attack
     *  3 : under enemy attack
     *  4 : destroyed
     */
    
    public function __construct($name,$materialproduction)
    {
        $this->name = $name;
        $this->materialproduction = $materialproduction;
    }
    
    public function setSector($sector)
    {
        $this->sector = $sector;
    }
    
    public function getSector()
    {
        return $this->sector;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function getName()
    {
        return $this->name;
    }
    
    public function getFreedBy()
    {
        return $this->freedBy;
    }
    
    public function setFreedBy($player)
    {
        $this->freedBy = $player;
    }
    
    public function setMaterial($material)
    {
        $this->materials = $material;
    }
    
    public function getMaterial()
    {
        return $this->materials;
    }
    
    public function setMaterialProduction($production)
    {
        $this->materialproduction = $production;
    }
    
    public function getMaterialProduction()
    {
        return $this->materialproduction;
    }
    
    public function setName($name)
    {
        $this->name = $name;
    }
    
    public function isAttacked()
    {
        return $this->isattacked;
    }
    
    public function attack()
    {
        $this->isattacked = true;
        $this->status = 2;
        $this->attackcount = $this->attackcount + 1;
    }
    
    public function unattack()
    {
        $this->isattacked = false;
    }
    
    public function setGarrison($array)
    {
        $this->garrison = $array;
    }
    
    public function getKnownGarrison()
    {
        return $this->knowngarrison;
    }
    
    public function getGarrison()
    {
        return $this->garrison;
    }
    
    public function setStatus($status)
    {
        $this->status = $status;
    }
    
    public function getStatus()
    {
        return $this->status;
    }
    
    public function spy()
    {
        $this->inteldate = time();
        $this->knowngarrison = $this->garrison;
    }
    
    public function getAttackCount()
    {
        return $this->attackcount;
    }
    
    public function reinitAttackCount()
    {
        $this->attackcount = 0;
    }
    
    public function getIntelDate()
    {
        return $this->inteldate;
    }
    
}