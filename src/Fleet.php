<?php
/**
 * @Entity @Table(name="fleets")
 **/
 
 use Doctrine\Common\Collections\ArrayCollection;
 
class Fleet
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text") **/
    protected $name;
    /** @OneToOne(targetEntity="Player",inversedBy="fleet") **/
    private $player;
    /** @Column(type="integer") **/
    protected $x=0;
    /** @Column(type="integer") **/
    protected $y=0;
    /** @Column(type="integer") **/
    protected $z=0;
    /**
     * @OneToMany(targetEntity="Ship", mappedBy="fleet", indexBy="id")
     * @var Ship[]
     **/
    private $ships;
	/**
     * @OneToMany(targetEntity="Character", mappedBy="fleet", indexBy="id")
     * @var Character[]
     **/
    private $characters;
    /** @Column(type="integer") **/
    protected $qualifiedStaff=0;
    /** @Column(type="integer") **/
    protected $food=500;
    /** @Column(type="integer") **/
    protected $fuel=250;
    /** @Column(type="integer") **/
    protected $medicine=100;
    /** @Column(type="integer") **/
    protected $material=1000;
    /** @Column(type="integer") **/
    protected $moral=40;
    /** @Column(type="integer") **/
    protected $jumpStatus=0;
    /** @OneToMany(targetEntity="HistoryItem", mappedBy="fleet", indexBy="id")
     * @var HistoryItem[]
     **/
    protected $history;
    /** @OneToOne(targetEntity="Ship") nullable=true **/
    private $admiralShip;
    /** @OneToMany(targetEntity="Ship", mappedBy="protectedByFleet", indexBy="id") nullable=true
     * @var Ship[]
     **/
    private $protectedShips;
    /** @Column(type="boolean") **/
    protected $specificProtection=false;
	/** @Column(type="integer",options={"default"=0}) **/
	protected $nbEnemiesKilled=0;
	/** @ManyToOne(targetEntity="PoliticalSystem") **/
	private $politicalSystem;
	/** @Column(type="integer",options={"default"=0})**/
	protected $corruption=0;
	/** @Column(type="boolean",options={"default"=0}) **/
	protected $abortionLaw=true;
	/** @Column(type="boolean",options={"default"=1}) **/
	protected $deathPenalty=false;
	/** @Column(type="boolean",options={"default"=1}) **/
	protected $martialLaw=false;
	/** @OneToOne(targetEntity="Character") **/
	private $chiefCharacter;
    
    /*
     * Jump status : 
     * 
     * -1 : not functionnal
     * 0 : not ready
     * 1 : set coordinates
     * 2 : transmitting new coordinates
     * 3 : warming FTL drive (up to 25%)
     * 4 : warming FTL drive (up to 50%)
     * 5 : warming FTL drive (up to 75%)
     * 6 : warming FTL drive (up to 100%)
     * 7 : synchronizing with fleet (up to 50%)
     * 8 : synchronizing with fleet (up to 75%)
     * 9 : synchronizing with fleet (up to 100%)
     * 10 : JUMP ! (should not appear or only for transitionnal state)
     */
    
    public function __construct($player)
    {
		$this->ships = new ArrayCollection();
		$this->history = new ArrayCollection();
		$this->protectedShips = new ArrayCollection();
		$this->setPlayer($player);
	}
    
    public function getId()
    {
		return $this->id;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function setPlayer($player)
	{
		$this->player=$player;
	}
	
	public function getPlayer()
	{
		return $this->player;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function getX()
	{
		return $this->x;
	}
	
	public function getY()
	{
		return $this->y;
	}
	
	public function getZ()
	{
		return $this->z;
	}
	
	public function setX($x)
	{
		$this->x=$x;
	}
	
	public function setY($y)
	{
		$this->y=$y;
	}
	
	public function setZ($z)
	{
		$this->z=$z;
	}

	public function getShips()
	{
		return $this->ships->toArray();
	}
	
	public function getShip($id)
	{
		if (!isset($this->ships[$id]))
		{
			echo "not in ships";
			return false;
		}
		else
		{
			return $this->ships[$id];
		}
	}
	
	public function addShip($ship)
	{
		$this->ships[$ship->getId()] = $ship;
	}
	
	public function getCharacters()
	{
		return $this->characters->toArray();
	}
	
	public function getCharacter($id)
	{
		if (!isset($this->characters[$id]))
		{
			return false;
		}
		else
		{
			return $this->characters[$id];
		}
	}
	
	public function addCharacter($character)
	{
		$this->characters[$character->getId()] = $character;
	}
	
	public function removeShip($ship)
	{
		
	}
	
	public function getAdmiralShip()
	{
		return $this->admiralShip;
	}
	
	public function setAdmiralShip($ship)
	{
		$this->admiralShip = $ship;
	}
	
	public function getChief()
	{
		return $this->chiefCharacter;
	}
	
	public function setChief($character)
	{
		$this->chiefCharacter = $character;
	}
	
	public function isEmpty()
	{
		$compo = $this->ships;
		return is_null($compo) || empty($compo) || count($compo) == 0;
	}
	
	public function setJumpStatus($status)
	{
		$this->jumpStatus=$status;
	}
	
	public function getJumpStatus()
	{
		return $this->jumpStatus;
	}
	
	public function getCombinedAttack()
	{
		$value = 0;
		foreach ($this->ships as $ship)
		{
			$level = $ship->getLevel();
			$value += $ship->getAttack($level);
		}
		return $value;
	}
	
	public function getMaterial()
	{
		return $this->material;
	}
	
	public function setMaterial($material)
	{
		if ($material < 0)
		{
			$material = 0;
		}
		$this->material=$material;
	}
	
	public function increaseMaterial($ammount)
	{
		$this->material += $ammount;
	}
	
	public function decreaseMaterial($ammount)
	{
		if ($ammount > $this->material)
		{
			throw new Exception("Not enough materials in fleet");
		}
		$this->material -= $ammount;
	}
	
	public function getFood()
	{
		return $this->food;
	}
	
	public function setFood($food)
	{
		if ($food < 0)
		{
			$food = 0;
		}
		$this->food = $food;
	}
	
	public function getFuel()
	{
		return $this->fuel;
	}
	
	public function setFuel($fuel)
	{
		if ($fuel < 0)
		{
			$fuel = 0;
		}
		$this->fuel = $fuel;
	}
	
	public function decreaseFuel($ammount)
	{
		if ($ammount > $this->fuel)
		{
			throw new Exception("Not enough fuel in fleet");
		}
		$this->fuel -= $ammount;
	}
	
	public function increaseFuel($ammount)
	{
		$this->fuel += $ammount;
	}
	
	public function getMedicine()
	{
		return $this->medicine;
	}
	
	public function setMedicine($med)
	{
		if ($med < 0)
		{
			$med = 0;
		}
		$this->medicine = $med;
	}
	
	public function increaseMedicine($ammount)
	{
		$this->medicine += $ammount;
	}
	
	public function decreaseMedicine($ammount)
	{
		if ($ammount > $this->medicine)
		{
			throw new Exception("Not enough medicine in the fleet");
		}
		$this->medicine -= $ammount;
	}
	
	public function getMoral()
	{
		return $this->moral;
	}
	
	public function setMoral($moral)
	{
		if ($moral < 0)
		{
			$moral = 0;
		}
		$this->moral = $moral;
	}
	
	public function decreaseMoral($ammount)
	{
		if ($ammount > $this->moral)
		{
			$ammount = $this->moral;
		}
		$this->moral -= $ammount;
	}
	
	public function increaseMoral($ammount)
	{
		$this->moral += $ammount;
		if ($this->moral > 100)
		{
			$this->moral = 100;
		}
	}
	
	public function getHistory()
	{
		return $this->history;
	}
	
	public function addShipToHistory($ship)
	{
		if (is_null($this->history))
		{
			$this->history = new ArrayCollection();
		}
		$histShip = new HistoryItem($ship);
		$this->history[$histShip->getId()] = $histShip;
	}
	
	public function getQualifiedStaff()
	{
		return $this->qualifiedStaff;
	}
	
	public function increaseQualifiedStaff($ammount)
	{
		$this->qualifiedStaff += $ammount;
	}
	
	public function decreaseQualifiedStaff($ammount)
	{
		if ($ammount > $this->qualifiedStaff)
		{
			throw new Exception("Not enough qualified staff");
		}
		$this->qualifiedStaff -= $ammount;
	}
	
	public function getSpecificProtection()
	{
		return $this->specificProtection;
	}
	
	public function setSpecificProtection($protection)
	{
		$this->specificProtection = $protection;
	}
	
	public function getProtectedShips()
	{
		return $this->protectedShips;
	}
	
	public function protectShip($ship)
	{
		$this->protectedShips[$ship->getId()] = $ship;
		$this->specificProtection=true;
		$ship->setProtection($this);
	}
	
	public function removeShipFromProtection($ship)
	{
		$this->protectedShips->remove($ship->getId());
		$ship->setProtection(null);
		if (count($this->protectedShips) == 0)
		{
			$this->specificProtection=false;
		}
	}
	
	public function globalProtection()
	{
		$ships = $this->protectedShips;
		foreach ($ships as $ship)
		{
			$this->protectedShips->remove($ship->getId());
			$ship->setProtection(null);
		}
		$this->specificProtection=false;
	}
	
	public function getCombinedDefense()
	{
		$value = 0;
		foreach ($this->ships as $ship)
		{
			$level = $ship->getLevel();
			// DO NOT change for $ship->getDefense($level) or it will loop forever
			$value += $ship->getType()->getDefense($level);
		}
		// as we use ShipType->getDefense() we have to add the fleet bonus for defense
		$fleetBonus=1;
		$politicalsystem = $this->getPoliticalSystem();
		if (!is_null($politicalsystem))
		{
			$fleetBonus = 1 + $politicalsystem->getDefenseBonus();
		}
		
		// the fleet does not take advantage of characters defense buff
		
		return round($value * $fleetBonus);
	}
	
	public function getSpecificDefenseValue()
	{
		return round($this->getCombinedDefense() / count($this->protectedShips));
	}
	
	public function setNbEnemiesKilled($value)
	{
		$this->nbEnemiesKilled=$value;
	}
	
	public function increaseNbEnemiesKilled($value=1)
	{
		$this->nbEnemiesKilled = $this->nbEnemiesKilled + $value;
	}
	
	public function getNbEnemiesKilled()
	{
		return $this->nbEnemiesKilled;
	}
	
	public function setPoliticalSystem($ps)
	{
		$this->politicalSystem = $ps;
	}
	
	public function getPoliticalSystem()
	{
		return $this->politicalSystem;
	}
	
	public function setCorruption($value)
	{
		if ($value > 100)
		{
			$value = 100;
		}
		$this->corruption=$value;
	}
	
	public function getCorruption()
	{
		return $this->corruption;
	}
	
	public function setAbortionLaw($value)
	{
		$this->abortionLaw=$value;
	}
	
	public function getAbortionLaw()
	{
		return $this->abortionLaw;
	}
	
	public function setDeathPenalty($value)
	{
		$this->deathPenalty=$value;
	}
	
	public function getDeathPenalty()
	{
		return $this->deathPenalty;
	}
	
	public function setMartialLaw($value)
	{
		$this->martialLaw = $value;
	}
	
	public function getMartialLaw()
	{
		return $this->martialLaw;
	}
}
