<?php
require_once __DIR__.'/../tools.php';
require_once __DIR__.'/../const.php';
/**
 * @Entity @Table(name="csrf")
 **/
class CSRF
{
    /** @Id @Column(type="string") **/
    protected $token;
    /** @Column(type="integer") **/
    protected $time;
    /** @Column(type="boolean") **/
    protected $active=true;
    
    public function __construct()
    {
		$this->token = Tools::randomString(TOKEN_LENGTH);
		$this->time = time();
		$this->active=true;
	}
	
	public function getToken()
	{
		return $this->token;
	}
	
	public function getTime()
	{
		return $this->time;
	}
	
	public function isActive()
	{
		$result = $this->active && $this->time >= (time() - TOKEN_TIMEOUT);
		$this->active=false;
		return $result;
	}
}
