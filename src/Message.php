<?php
/**
 * @Entity @Table(name="messages")
 **/
 
class Message
{
	/** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @ManyToOne(targetEntity="Player") nullable=true**/
    private $sender;
    /** @ManyToOne(targetEntity="Player") **/
    private $recipient;
    /** @Column(type="string") **/
    protected $message;
    /** @Column(type="boolean") **/
    protected $international;
    /** @Column(type="boolean") **/
    protected $read=false;
    /** @Column(type="boolean") **/
    protected $senderdeleted=false;
    /** @Column(type="boolean") **/
    protected $recipientdeleted=false;
    /** @Column(type="integer") **/
    protected $time=0;
    
    public function __construct($sender,$recipient,$message,$internationalized=false)
    {
		$this->sender = $sender;
		$this->recipient = $recipient;
		$this->message = $message;
		$this->international = $internationalized;
		$this->time = time();
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function read()
	{
		$this->read = true;
	}
	
	public function isRead()
	{
		return $this->read;
	}
	
	public function getMessage($i18n=null)
	{
		if ($this->international)
		{
			return $i18n->getText($this->message);
		}
		return $this->message;
	}
	
	public function getSender()
	{
		return $this->sender;
	}
	
	public function getRecipient()
	{
		return $this->recipient;
	}
	
	public function getTime($format=null)
	{
		if (is_null($format))
		{
			$format = 'd/m/y H:i:s';
		}
		return date($format,$this->time);
	}
	
	public function senderDelete()
	{
		$this->senderdeleted=true;
	}
	
	public function recipientDelete()
	{
		$this->recipientdeleted=true;
	}
	
	public function getTruncatedMessage($length,$i18n=null)
	{
		if ($this->international)
		{
			return substr($i18n->getText($this->message),0,$length);
		}
		return substr($this->message,0,$length);
	}
}
