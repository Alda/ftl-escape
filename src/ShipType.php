<?php
require_once __DIR__.'/../const.php';
/**
 * @Entity @Table(name="shiptypes")
 **/
class ShipType
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text", unique=true) **/
    protected $name;
    /** @Column(type="integer") **/
    protected $maxHp=1;
    /** @Column(type="integer") **/
    protected $defense=0;
    /** @Column(type="integer") **/
    protected $attack=0;
    /** @Column(type="integer") **/
    protected $maxPassengers=100;
    /** @Column(type="integer") **/
    protected $qualifiedStaff=2;
    /** @Column(type="float") **/
    protected $foodProduction=0.0;
    /** @Column(type="float") **/
    protected $fuelProduction=0.0;
    /** @Column(type="float") **/
    protected $medicineProduction=0.0;
    /** @Column(type="float") **/
    protected $materialProduction=0.0;
    /** @Column(type="float") **/
    protected $moralProduction=0.1;
    /** @Column(type="integer") **/
    protected $qualifiedStaffPerCycle=0;
    /** @Column(type="boolean") **/
    protected $scout = false;
    /** @Column(type="boolean") **/
    protected $produceShips = false;
    /** @Column(type="boolean") **/
    protected $repair = false;
    /** @Column(type="boolean") **/
    protected $mine = false;
    /** @Column(type="integer") **/
    protected $price=10000;
    /** @Column(type="string") **/
    protected $nameDico="civilians";
    /** @Column(type="boolean") **/
    protected $buildable=true;
    /** @Column(type="boolean") **/
    protected $uniqueShip=false;
    
    public function getId()
    {
		return $this->id;
	}
	
	public function setName($name)
	{
		$this->name = $name;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function getDescription()
	{
		return $this->name.'.description';
	}
	
	public function setDamage($damage)
	{
		$this->attack=$damage;
	}
	
	public function getBuildable()
	{
		return $this->buildable;
	}
	
	public function isBuildable()
	{
		return $this->getBuildable();
	}
	
	public function setBuildable($buildable)
	{
		$this->buildable = $buildable;
	}
	
	public function isUnique()
	{
		return $this->uniqueShip;
	}
	
	public function setUnique($unique)
	{
		$this->uniqueShip = $unique;
	}
	
	public function setAttack($damage)
	{
		$this->setDamage($damage);
	}
	
	public function getDamage($level=0)
	{
		return round($this->attack * pow(SHIP_LEVEL_MULTIPLIER,$level));
	}
	
	public function getAttack($level=0)
	{
		return $this->getDamage($level);
	}
	
	public function setMaxHP($hp)
	{
		$this->maxHp=$hp;
	}
	
	public function getMaxHP($level=0)
	{
		return round($this->maxHp * pow(SHIP_LEVEL_MULTIPLIER,$level));
	}
	
	public function setDefense($defense)
	{
		$this->defense=$defense;
	}
	
	public function getDefense($level=0)
	{
		return round($this->defense * pow(SHIP_LEVEL_MULTIPLIER,$level));
	}
	
	public function setMaxPassengers($passengers)
	{
		$this->maxPassengers=$passengers;
	}
	
	public function getMaxPassengers()
	{
		return $this->maxPassengers;
	}
	
	public function setQualifiedStaff($staff)
	{
		$this->qualifiedStaff=$staff;
	}
	
	public function getQualifiedStaff()
	{
		return $this->qualifiedStaff;
	}
	
	public function setFoodProduction($production)
	{
		$this->foodProduction=$production;
	}
	
	public function getFoodProduction($level=0)
	{
		return round($this->foodProduction * pow(SHIP_LEVEL_MULTIPLIER,$level),PHP_ROUND_HALF_DOWN);
	}
	
	public function setFuelProduction($production)
	{
		$this->fuelProduction=$production;
	}
	
	public function getFuelProduction($level=0)
	{
		return round($this->fuelProduction * pow(SHIP_LEVEL_MULTIPLIER,$level),PHP_ROUND_HALF_DOWN);
	}
	
	public function setMaterialProduction($production)
	{
		$this->materialProduction=$production;
	}
	
	public function getMaterialProduction($level=0)
	{
		return round($this->materialProduction * pow(SHIP_LEVEL_MULTIPLIER,$level),PHP_ROUND_HALF_DOWN);
	}
	
	public function setMedicineProduction($production)
	{
		$this->medicineProduction=$production;
	}
	
	public function getMedicineProduction($level=0)
	{
		return round($this->medicineProduction * pow(SHIP_LEVEL_MULTIPLIER,$level),PHP_ROUND_HALF_DOWN);
	}
	
	public function setMoralProduction($production)
	{
		$this->moralProduction=$production;
	}
	
	public function getMoralProduction()
	{
		return $this->moralProduction;
	}
	
	public function setQualifiedStaffPerCycle($production)
	{
		$this->qualifiedStaffPerCycle=$production;
	}
	
	public function getQualifiedStaffPerCycle($level=0)
	{
		return round($this->qualifiedStaffPerCycle * pow(SHIP_LEVEL_MULTIPLIER,$level),PHP_ROUND_HALF_DOWN);
	}
	
	public function enableScout()
	{
		$this->scout = true;
	}
	
	public function canScout()
	{
		return $this->scout;
	}
	
	public function enableProduceShips()
	{
		$this->produceShips = true;
	}
	
	public function canProduceShips()
	{
		return $this->produceShips;
	}
	
	public function enableRepair()
	{
		$this->repair = true;
	}
	
	public function canRepair()
	{
		return $this->repair;
	}
	
	public function enableMining()
	{
		$this->mine = true;
	}
	
	public function canMine()
	{
		return $this->mine;
	}
	
	public function setPrice($price)
	{
		$this->price=$price;
	}
	
	public function getPrice()
	{
		return $this->price;
	}
	
	public function setNameDico($name)
	{
		$this->nameDico = $name;
	}
	
	public function getNameDico()
	{
		return $this->nameDico;
	}
	
	public function getFormatedPrice($decPoint='.',$thousandsSep=' ')
	{
		return number_format($this->getPrice(),0,$decPoint,$thousandsSep);
	}
}
