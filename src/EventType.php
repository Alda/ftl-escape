<?php
require_once __DIR__.'/../tools.php';
require_once __DIR__.'/../const.php';
require_once __DIR__.'/../builder.php';
require_once(__DIR__.'/../lib/i18n.php');
/**
 * @Entity @Table(name="eventtypes")
 **/
 use Doctrine\Common\Collections\ArrayCollection;
class EventType
{
    /** @Id @Column(type="integer") **/
    protected $id;
    
    /** @Column(type="string") **/
    protected $name;
    
    /** @Column(type="array", nullable=true) **/
    protected $answers;
    
    /** @Column(type="array", nullable=true) **/
    protected $conditions;
    
    /* answers format :
     * array (
     * 		x => array (
     * 					"name"=>"event.$id.answer.x",
     * 					"resultmsg"=>"msg.event.custom.answer.x", // nullable
     * 					"effects"=>array(
     * 									"moral"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"fuel"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"food"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"medicine"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"staff"=>array("min"=>INTEGER,"max"=>INTEGER), // concerns only staff stock
     * 									"material"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"passengers"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"ships"=>array("min"=>INTEGER,"max"=>INTEGER), // number of NEW ships
     * 									"ennemies"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"nbshipsdamaged"=>array("min"=>INTEGER,"max"=>INTEGER), // number of ships damaged, needs "damages"
     * 									"damages"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"possibleambush"=>array("chance"=>INTEGER,"over"=>INTEGER,"minennemies"=>INTEGER,"maxennemies"=>INTEGER,"minallies"=>INTEGER,"maxallies"=>INTEGER), // has $chance over $over to be ennemies, $chance over $over to be allies, or nothing
     * 									)
     * 					)
     * 		)
     */
     
     /* conditions format :
      * array (
      * 		"moral"=>array("min"=>INTEGER,"max"=>INTEGER),
      * 		"fuel"=>array("min"=>INTEGER,"max"=>INTEGER),
      * 		"food"=>array("min"=>INTEGER,"max"=>INTEGER),
      * 		"medicine"=>array("min"=>INTEGER,"max"=>INTEGER),
      * 		"material"=>array("min"=>INTEGER,"max"=>INTEGER),
      * 		"nbships"=>array("min"=INTEGER,"max"=>INTEGER),
      * 		"survivors"=>array("min"=>INTEGER,"max"=>INTEGER), // not implemented
      * 		"objective"=>INTEGER, // not implemented
      * 		"objectiveaims"=>array("min"=>INTEGER,"max"=>INTEGER) // not implemented
      * 		)
      */
    
    public function __construct($id,$name,$answers,$conditions=null)
    {
		$this->id=$id;
		$this->name=$name;
		$this->answers=$answers;
		$this->conditions=$conditions;
	}
    
    public function getId()
    {
		return $this->id;
	}
	
	public function getAnswers()
	{
		return $this->answers;
	}
	
	public function getName()
	{
		return $this->name;
	}
	
	public function matchConditions($player)
	{
		if (is_null($this->conditions))
		{
			return true;
		}
		$conditions = $this->conditions;
		$fleet = $player->getFleet();
		$sector = $player->getSector();
		$matchConditions = true;
		foreach ($conditions as $key=>$values)
		{
			switch ($key)
			{
				case 'moral': $matchConditions = $matchConditions && $fleet->getMoral() >= $values['min'] && $fleet->getMoral() <= $values['max']; break;
				case 'fuel': $matchConditions = $matchConditions && $fleet->getFuel() >= $values['min'] && $fleet->getFuel() <= $values['max']; break;
				case 'food': $matchConditions = $matchConditions && $fleet->getFood() >= $values['min'] && $fleet->getFood() <= $values['max']; break;
				case 'medicine': $matchConditions = $matchConditions && $fleet->getMedicine() >= $values['min'] && $fleet->getMedicine() <= $values['max']; break;
				case 'material': $matchConditions = $matchConditions && $fleet->getMaterial() >= $values['min'] && $fleet->getMaterial() <= $values['max']; break;
				case 'nbships': $matchConditions = $matchConditions && count($fleet->getShips()) >= $values['min'] && count($fleet->getShips()) <= $values['max']; break;
			}
		}
		return $matchConditions;
	}
	
	private function resolveMoral($player,$value)
	{
		$fleet = $player->getFleet();
		$fleet->setMoral($fleet->getMoral() + $value);
		return $value;
	}
	
	private function resolveFuel($player,$value)
	{
		$fleet = $player->getFleet();
		$fleet->setFuel($fleet->getFuel() + $value);
		return $value;
	}
	
	private function resolveFood($player,$value)
	{
		$fleet = $player->getFleet();
		$fleet->setFood($fleet->getFood() + $value);
		return $value;
	}
	
	private function resolveMedicine($player,$value)
	{
		$fleet = $player->getFleet();
		$fleet->setMedicine($fleet->getMedicine() + $value);
		return $value;
	}
	
	private function resolveStaff($player,$value)
	{
		$fleet = $player->getFleet();
		$fleet->setQualifiedStaff($fleet->getQualifiedStaff() + $value);
		return $value;
	}
	
	private function resolveMaterial($player,$value)
	{
		$fleet = $player->getFleet();
		$fleet->setMaterial($fleet->getMaterial() + $value);
		return $value;
	}
	
	private function resolvePassengers($player,$value)
	{
		// TODO
		return 'TODO';
	}
	
	private function resolveShips($player,$value)
	{
		$fleet = $player->getFleet();
		Builder::populateFleet($fleet,false,$value);
		return $value;
	}
	
	private function resolveEnnemies($player,$value)
	{
		$sector = $player->getSector();
		Builder::populateSector($sector,$value);
		return $value;
	}
	
	private function resolveShipsDamages($player,$nb,$min,$max)
	{
		// TODO
		return 'TODO';
	}
	
	public function resolveEvent($answerId,$player)
	{
		$answers = $this->answers;
		if (array_key_exists($answerId,$answers))
		{
			$result = array();
			$effects = $answers[$answerId]['effects'];
			foreach ($effects as $key => $values)
			{
				switch ($key)
				{
					case 'moral': $result[$key] = $this->resolveMoral($player,rand($values['min'],$values['max'])); break;
					case 'fuel': $result[$key] = $this->resolveFuel($player,rand($values['min'],$values['max'])); break;
					case 'food': $result[$key] = $this->resolveFood($player,rand($values['min'],$values['max'])); break;
					case 'medicine': $result[$key] = $this->resolveMedicine($player,rand($values['min'],$values['max'])); break;
					case 'staff': $result[$key] = $this->resolveStaff($player,rand($values['min'],$values['max'])); break;
					case 'material': $result[$key] = $this->resolveMaterial($player,rand($values['min'],$values['max'])); break;
					case 'passengers': $result[$key] = $this->resolvePassengers($player,rand($values['min'],$values['max'])); break;
					case 'ships': $result[$key] = $this->resolveShips($player,rand($values['min'],$values['max'])); break;
					case 'ennemies': $result[$key] = $this->resolveEnnemies($player,rand($values['min'],$values['max'])); break;
					case 'nbshipsdamaged': $result[$key] = $this->resolveShipsDamages($player,rand($values['min'],$values['max']),$effects['damages']['min'],$effects['damages']['max']);break;
					case 'possibleambush':
						$event = false;
						$dice = rand(1,$values['over']);
						if ($dice <= $values['chance'])
						{
							$result[$key]['ennemies'] = $this->resolveEnnemies($player,$values['minennemies'],$values['maxennemies']);
							$event = true;
						}
						$dice = rand(1,$values['over']);
						if ($dice <= $values['chance'])
						{
							$result[$key]['allies'] = $this->resolveShips($player,$values['minallies'],$values['maxallies']);
							$event = true;
						}
						if (!$event)
						{
							$result[$key] = null;
						}
						break;
				}
			}
			return $this->buildResults($result,array_key_exists('resultmsg',$answers[$answerId]) ? $answers[$answerId]['resultmsg'] : null);
		}
		else
		{
			return false;
		}
	}
	
	private function buildResults($rawResults,$resultmsg)
	{
		$i18n = new I18n();
		$i18n->autoSetLang();
		if (is_null($resultmsg))
		{
			foreach ($rawResults as $key=>$rawResult)
			{
				switch ($key)
				{
					case 'moral': Tools::setFlashMsg($i18n->getText('msg.event.moral',array($rawResults[$key])));break;
					case 'fuel': Tools::setFlashMsg($i18n->getText('msg.event.fuel',array($rawResults[$key])));break;
					case 'food': Tools::setFlashMsg($i18n->getText('msg.event.food',array($rawResults[$key])));break;
					case 'medicine': Tools::setFlashMsg($i18n->getText('msg.event.medicine',array($rawResults[$key])));break;
					case 'staff': Tools::setFlashMsg($i18n->getText('msg.event.staff',array($rawResults[$key])));break;
					case 'material': Tools::setFlashMsg($i18n->getText('msg.event.material',array($rawResults[$key])));break;
					case 'passengers': Tools::setFlashMsg($i18n->getText('msg.event.passengers',array($rawResults[$key])));break;
					case 'ships': Tools::setFlashMsg($i18n->getText('msg.event.ships',array($rawResults[$key])));break;
					case 'ennemies': Tools::setFlashMsg($i18n->getText('msg.event.ennemies',array($rawResults[$key])));break;
					case 'nbshipsdamaged': Tools::setFlashMsg($i18n->getText('msg.event.nbshipsdamaged',$rawResults[$key]));break;
					case 'possibleambush': Tools::setFlashMsg($i18n->getText('msg.event.possibleambush',array($rawResults[$key])));break;
				}
			}
		}
		else
		{
			Tools::setFlashMsg($i18n->getText($resultmsg,$rawResults));
		}
		return $rawResults;
	}
}

