<?php
/**
 * @Entity @Table(name="politicalsystems")
 **/
 
 use Doctrine\Common\Collections\ArrayCollection;
 
class PoliticalSystem
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @Column(type="text") **/
    protected $name;
    /** @Column(type="integer", options={"default"=0}) **/
    protected $liberty=0;
    /** @Column(type="integer", options={"default"=0}) **/
    protected $equality=0;
    /** @Column(type="text", nullable=true) **/
    protected $religion;
    /** @Column(type="boolean", options={"default"=0}) **/
    protected $election=true;
    /** @Column(type="float", options={"default"=0}) **/
    protected $productionBonus=0.0;
    /** @Column(type="float", options={"default"=0}) **/
    protected $moralBonus=0.0;
    /** @Column(type="float", options={"default"=0}) **/
    protected $defenseBonus=0.0;
    /** @Column(type="float", options={"default"=0}) **/
    protected $attackBonus=0.0;
    /** @Column(type="float", options={"default"=0}) **/
    protected $corruptionBonus=0.0;
    /** @Column(type="boolean", options={"default"=0}) **/
    protected $independantJustice=true;
    /** @Column(type="integer", options={"default"=10}) **/
    protected $populationControl=10;
    
    public function setName($name)
    {
        $this->name=$name;
    }

    public function getId(){
        return $this->id;
    }

    public function getName(){
        return $this->name;
    }

    public function setLiberty($value){
        if ($value > 100)
        {
            $value=100;
        }
        $this->liberty = $value;
    }

    public function getLiberty(){
        return $this->liberty;
    }

    public function setEquality($value){
        if ($value > 100)
        {
            $value=100;
        }
        $this->equality = $value;
    }

    public function getEquality(){
        return $this->equality;
    }

    public function setReligion($value){
        $this->religion = $value;
    }

    public function getReligion(){
        return $this->religion;
    }

    public function setElection($value){
        $this->election = $value;
    }

    public function getElection(){
        return $this->election;
    }

    public function setProductionBonus($value){
        $this->productionBonus = $value;
    }

    public function getProductionBonus(){
        return $this->productionBonus;
    }

    public function setMoralBonus($value){
        $this->moralBonus = $value;
    }

    public function getMoralBonus(){
        return $this->moralBonus;
    }

    public function setDefenseBonus($value){
        $this->defenseBonus = $value;
    }

    public function getDefenseBonus(){
        return $this->defenseBonus;
    }

    public function setAttackBonus($value){
        $this->attackBonus = $value;
    }
    
    public function getAttackBonus(){
        return $this->attackBonus;
    }
    
    public function setCorruptionBonus($value)
    {
        $this->corruptionBonus=$value;
    }
    
    public function getCorruptionBonus()
    {
        return $this->corruptionBonus;
    }
    
    public function setIndependantJustice($value)
    {
        $this->independantJustice=$value;
    }
    
    public function getIndependantJustice()
    {
        return $this->independantJustice;
    }
    
    public function setPopulationControl($value)
    {
        if ($value > 100)
        {
            $value = 100;
        }
        $this->populationControl=$value;
    }
    
    public function getPopulationControl()
    {
        return $this->populationControl;
    }
    
    public function getDefinition()
    {
        return $this->name.'.definition';
    }
    
}