<?php
/**
 * @Entity @Table(name="sectors")
 **/
 use Doctrine\Common\Collections\ArrayCollection;
class Sector
{
    /** @Id @Column(type="integer") @GeneratedValue **/
    protected $id;
    /** @OneToOne(targetEntity="Player") **/
    private $player;
    /** @Column(type="integer") **/
    protected $material=100;
    /**
     * @OneToMany(targetEntity="EnnemyShip", mappedBy="sector", indexBy="id")
     * @var Ship[]
     **/
    private $ennemies;
    /** @Column(type="string") **/
    protected $name="";
    /** @Column(type="integer") **/
    protected $wrecks=0;
    /** @Column(type="integer") **/
    protected $searched=0;
    /** @Column(type="boolean") **/
    protected $is_habitable=false;
	/** @OneToOne(targetEntity="Planet",inversedBy="sector") **/
	private $planet;
    
    public function __construct($player,$planet=null)
    {
		$this->player=$player;
		$this->ennemies = new ArrayCollection();
		$player->setSector($this);
		$this->planet = $planet;
		if (!is_null($planet))
		{
			$planet->setSector($this);
		}
	}
    
    public function getId()
    {
		return $this->id;
	}
	
	public function setMaterial($material)
	{
		$this->material=$material;
	}
	
	public function getMaterial()
	{
		return $this->material;
	}
	
	// should not be used
	public function addEnnemy($ship)
	{
		$this->ennemies[$ship->getId()]=$ship;
		$ship->setSector($this);
	}
	
	public function getEnnemies()
	{
		return $this->ennemies;
	}
	
	public function addWrecks($nb=1)
	{
		$this->wrecks+=$nb;
	}
	
	public function getWrecks()
	{
		return $this->wrecks;
	}
	
	public function removeWrecks($nb=1)
	{
		$this->wrecks-=$nb;
		if ($this->wrecks < 0)
		{
			// @TODO : better throw Exception there
			$this->wrecks = 0;
		}
	}
	
	public function getPlayer()
	{
		return $this->player;
	}
	
	public function getPlanet()
	{
		return $this->planet;
	}
	
	public function increaseSearch($value)
	{
		$this->searched+=$value;
		if ($this->searched > 100)
		{
			$this->searched = 100;
		}
	}
	
	public function getSearched()
	{
		return $this->searched;
	}
	
	public function isHabitable()
	{
		return $this->is_habitable;
	}
	
	public function setHabitable()
	{
		$this->is_habitable=true;
	}
}
