<?php
/**
 * @Entity @Table(name="game")
 **/
class Game
{
	/** @Id @Column(type="integer") **/
	protected $id;
	/** @Column(type="boolean") **/
	protected $init=false;
	/** @Column(type="boolean", options={"default" = 1}) **/
	protected $signup=true;
	
	public function __construct()
	{
		$this->id=1;
	}
	
	public function getId()
	{
		return $this->id;
	}
	
	public function getInit()
	{
		return $this->init;
	}
	
	public function init()
	{
		$this->init = true;
	}
	
	public function setSignup($signup)
	{
		$this->signup = $signup;
	}
	
	public function getSignup()
	{
		return $this->signup;
	}
}
