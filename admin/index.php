<?php

require_once(__DIR__.'/../bootstrap.php');
require_once(__DIR__.'/../const.php');
require_once(__DIR__.'/../tools.php');
require_once(__DIR__.'/../lib/i18n.php');

$players = $entityManager->getRepository('Player')->findAll();
$i18n = new I18n();
$i18n->autoSetLang();

foreach ($players as $player)
{
	if ($player->isGameOver())
	{
		echo '<font color="#FF0000">';
	}
	echo $player->getLogin();
	if ($player->isGameOver())
	{
		echo '</font>';
	}
	echo ' ('.count($player->getFleet()->getShips()).' vaisseaux)';
	$survivors = 0;
	$ships = $player->getFleet()->getShips();
	foreach ($ships as $ship)
	{
		$survivors += $ship->getPassengers() + $ship->getStaff();
	}
	echo ' ('.number_format($survivors,0,'.',' ').' survivants)';
	$sector = $player->getSector();
	if (is_null($sector))
	{
		echo 'NO SECTOR';
	}
	echo '<br />';
}

$ships = $entityManager->getRepository('ShipType')->findAll();
/*
?>
<table>
	<tr><td>Nom</td><td>HP</td><td>Attaque</td><td>Défense</td><td>Food</td><td>Fuel</td><td>Materiaux</td><td>Médocs</td><td>Prix</td></tr>
<?php
	foreach ($ships as $ship)
	{
		echo '<tr>
		<td>'.$i18n->getText($ship->getName()).'</td>
		<td>'.$ship->getHP().'</td>
		</tr>';
	}
?>
</table>
*/
