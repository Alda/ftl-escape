<?php

require_once __DIR__."/bootstrap.php";
require_once __DIR__."/const.php";
require_once __DIR__."/builder.php";
require_once __DIR__."/tools.php";

function sanitize_username($username)
{
	return htmlentities($username);
}

$status = 0;

$game = $entityManager->getRepository('Game')->find(1);
if($game->getSignup())
{
	$username = sanitize_username($_POST['username']);
	$password = $_POST['spassword'];
	
	if (Tools::login_exists($username))
	{
		$status=4;
	}
	else
	{
		if (mb_strlen($username) >= LOGIN_MIN_CHAR && $_POST['spassword'] === $_POST['rspassword'])
		{
			if ($_POST['email'] === '' || filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
			{
			$player = new Player();
			$player->setLogin($username);
			$player->setPassword($password);
			if ($_POST['email'] != '')
			{
				$player->setEmail($_POST['email']);
			}
			$fleet = Builder::buildFleet($player);
	
			$sector = Builder::buildSector($player,true);
	
			$entityManager->persist($sector);
	
			$entityManager->persist($player);
			$entityManager->persist($fleet);
			
			// building characters
			Builder::buildAllCharacters($fleet);
			
			$entityManager->flush();
			$status=1;
			}
			else
			{
				$status=5;
			}
		}
		else
		{
			if (mb_strlen($username) < LOGIN_MIN_CHAR)
			{
				$status=2;
			}
			elseif ($_POST['spassword'] != $_POST['rspassword'])
			{
				$status=3;
			}
			else
			{
				$status=666;
			}
		}
	}
}
else
{
	
	$status = 7;
}
header('Location: index.php?status='.$status);
