<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$survivors = 0;
	$notFullShips = array();
	$ships = $player->getFleet()->getShips();
	$maxSurvivors = 0;
	foreach ($ships as $ship)
	{
		$survivors += $ship->getPassengers() + $ship->getStaff();
		$maxSurvivors += $ship->getPassengers();
		if ($ship->getPassengers() < $ship->getType()->getMaxPassengers())
		{
			array_push($notFullShips,$ship);
		}
	}
	$birth = round($survivors/BIRTH_RATE);
	$nbShips = count($notFullShips);
	if ($nbShips > 0)
	{
		$rand = rand(0,$nbShips-1);
		$ship = $notFullShips[$rand];
		if (($ship->getPassengers()+$birth) > $ship->getType()->getMaxPassengers())
		{
			$ship->setPassengers($ship->getType()->getMaxPassengers());
		}
		else
		{
			$ship->setPassengers($ship->getPassengers()+$birth);
		}
	}
}
$entityManager->flush();
