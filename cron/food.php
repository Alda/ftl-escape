<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$fleet = $player->getFleet();
	$ships = $fleet->getShips();
	$politicalsystem = $fleet->getPoliticalSystem();
	$production=0;
	$humans=0;
	foreach ($ships as $ship)
	{
		$humans += $ship->getPassengers() + $ship->getStaff();
		$production += round ($ship->getType()->getFoodProduction($ship->getLevel()) * $ship->getEfficiency());
	}
	$bonus=1;
	if (!is_null($politicalsystem))
	{
		$bonus = 1 + $politicalsystem->getProductionBonus();
	}
	$production = $production * $bonus;
	$conso = $humans / FOOD_CONSUMPTION;
	$left = $fleet->getFood() - $conso + $production;
	if ($left < 0)
	{
		foreach($ships as $ship)
		{
			$leftPassengers = round($ship->getPassengers()*(1-KILL_PER_CENT_IN_FAMINE));
			$ship->setPassengers($leftPassengers);
			$leftStaff = round($ship->getStaff()*(1-KILL_PER_CENT_IN_FAMINE));
			$ship->setStaff($leftStaff);
		}
		$left = 0;
	}
	$fleet->setFood($left);
}

$entityManager->flush();
