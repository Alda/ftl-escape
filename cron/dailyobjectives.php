<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$objective = $player->getObjectiveType();
	if ($objective == OBJECTIVE_SEARCH_HABITABLE_ID)
	{
		$player->getFleet()->decreaseMoral(OBJECTIVE_SEARCH_HABITABLE_MORAL_LOSE_PER_DAY);
	}
	elseif ($objective == OBJECTIVE_SEARCH_EARTH_ID)
	{
		$player->getFleet()->decreaseMoral(OBJECTIVE_SEARCH_EARTH_MORAL_LOSE_PER_DAY);
	}
}
$entityManager->flush();
