<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$fleet = $player->getFleet();
	$ships = $fleet->getShips();
	$politicalsystem = $fleet->getPoliticalSystem();
	$fuelproduction=0;
	foreach ($ships as $ship)
	{
		$fuelproduction += round($ship->getType()->getFuelProduction($ship->getLevel()) * $ship->getEfficiency());
	}
	$bonus=1;
	if (!is_null($politicalsystem))
	{
		$bonus = 1 + $politicalsystem->getProductionBonus();
	}
	$fuelproduction = $fuelproduction * $bonus;
	$fleet->increaseFuel($fuelproduction);
	if ($fleet->getJumpStatus() >= MIN_JUMP_STATUS_FOR_JUMP)
	{
		$fleet->decreaseFuel(round(count($fleet->getShips())/NB_SHIPS_FOR_FUEL_CONSUMPTION));
	}
	if ($fleet->getFuel() == 0)
	{
		$fleet->setJumpStatus(MIN_JUMP_STATUS_FOR_JUMP - 1);
	}
}

$entityManager->flush();
