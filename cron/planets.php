<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$planets = $entityManager->getRepository('Planet')->findAll();

foreach ($planets as $planet)
{
	$planet->setMaterial($planet->getMaterial() + $planet->getMaterialProduction());
    if ($planet->getMaterial() > 1000 && $planet->getStatus() == PLANET_STATUS_OCCUPIED)
    {
        // @TODO : change this
        $nbAvailableShips = count($entityManager->getRepository('EnnemyShipType')->findAll());
		$randId = rand(1,$nbAvailableShips);
        
        $garrison = $planet->getGarrison();
        $garrison[$randId] = $garrison[$randId] + 1;
        $planet->setGarrison($garrison);
        
        $planet->setMaterial($planet->getMaterial() - 1000);
    }
}
$entityManager->flush();
