<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');
require_once($docroot.'/builder.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$sector = $player->getSector();
	if (!is_null($sector))
	{
		$ennemies = $sector->getEnnemies();
		if (count($ennemies) == 0)
		{
			$dice = rand(1,100);
			if ($dice <= CHANCE_OF_EVENT)
			{
				$fleet = $player->getFleet();
				$qb = $entityManager->createQueryBuilder();
				$qb->select('s')
					->from('ShipType','s')
					->where('s.buildable = ?1')
					->andWhere('s.id != 1')
					->setParameter(1,true);
				$availableShips = $qb->getQuery()->getResult();
				$nbAvailableShips = count($availableShips);
				$randType = $availableShips[rand(0,$nbAvailableShips-1)];
				$return = Builder::buildShip($fleet,$randType,rand(0,$randType->getMaxPassengers()),$randType->getQualifiedStaff());
				if ($return != -1)
				{
					$message = new Message(null,$player,'msg.encounter.new.ship',true);
					$entityManager->persist($message);
				}
			}
		}
	}
}

$entityManager->flush();
