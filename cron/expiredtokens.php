<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');

$qb = $entityManager->createQueryBuilder();
$qb->delete('CSRF','t')
    ->where('t.time < :time')
    ->setParameter('time',time() - TOKEN_TIMEOUT);
$qb->getQuery()->execute();
$entityManager->flush();