<?php
$docroot = '';
if (php_sapi_name() == "cli") {
	$path = pathinfo(__FILE__);
	$path = $path['dirname'];
	$docroot = realpath($path.'/../');
}
else
{
	$docroot = __DIR__.'/..';
}
require_once($docroot.'/bootstrap.php');
require_once($docroot.'/const.php');
require_once($docroot.'/tools.php');
require_once($docroot.'/helper.php');

$players = $entityManager->getRepository('Player')->findAll();

foreach ($players as $player)
{
	$sector = $player->getSector();
	if (!is_null($sector))
	{
		if (count($sector->getEnnemies()) == 0)
		{
			$dice = rand(1,100);
			$chance = CHANCE_OF_ATTACK + round((time() - $player->getLastAttack()) / 3600);
			if ($dice <= $chance)
			{
				$nbEnnemies = rand(1,MAX_ENNEMIES_PER_SECTOR);
				$maxdifficulty = Helper::getDifficulty($player);
				echo $player->getName().' : '.$maxdifficulty.'<br />';
				$qb = $entityManager->createQueryBuilder();
				$qb->select('s')
					->from('EnnemyShipType','s')
					->where('s.difficulty <= ?1')
					->setParameter(1,$maxdifficulty);
				$availableShips = $qb->getQuery()->getResult();
				$nbAvailableShips = count($availableShips);
				for ($num=0;$num<$nbEnnemies;$num++)
				{
					$randType = $availableShips[rand(0,$nbAvailableShips - 1)];
					$ennemyShip = new EnnemyShip($sector,$randType,'Bandit '.$num);
					$ennemyShip->setHP($randType->getMaxHP());
					$entityManager->persist($ennemyShip);
				}
			}
		}
	}
}

$entityManager->flush();
