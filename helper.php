<?php

require_once(__DIR__.'/const.php');

class Helper
{
	static function canScout($fleet)
	{
		$ships = $fleet->getShips();
		$canScout = false;
		foreach($ships as $ship)
		{
			$canScout = $canScout || $ship->getType()->canScout();
		}
		return $canScout;
	}
	
	static function calculateDifficulty($player)
	{
		$fleet = $player->getFleet();
		$attack = $fleet->getCombinedAttack();
		$ships = $fleet->getShips();
		$defense = 0;
		foreach($ships as $ship)
		{
			$defense += $ship->getDefense();
		}
		$difficulty = round(($attack * DIFFICULTY_CALC_MULTIPLIER_ATTACK + $defense * DIFFICULTY_CALC_MULTIPLIER_DEFENSE) / DIFFICULTY_CALC_DIVIDER,0);
		if ($difficulty > MAX_DIFFICULTY)
		{
			$difficulty = MAX_DIFFICULTY;
		}
		return $difficulty;
	}
	
	static function getDifficulty(Player $player)
	{
		$difficulty = $player->getDifficulty();
		if (is_null($difficulty))
		{
			$difficulty = Helper::calculateDifficulty($player);
		}
		return $difficulty;
	}
	
	static function canProduce($fleet)
	{
		$ships = $fleet->getShips();
		$canProduce = false;
		while (!$canProduce && list(,$ship) = each($ships))
		{
			$canProduce = $canProduce || $ship->getType()->canProduceShips();
		}
		return $canProduce;
	}
	
	static function canRepair($fleet)
	{
		$ships = $fleet->getShips();
		$canRepair = false;
		while (!$canRepair && list(,$ship) = each($ships))
		{
			$canRepair = $canRepair || $ship->getType()->canRepair();
		}
		return $canRepair;
	}
	
	static function hasEvent($player)
	{
		return !is_null($player->getEvent());
	}
	
	static function canAct($player)
	{
		return !Helper::underAttack($player) && !Helper::hasEvent($player);
	}
	
	static function underAttack($player)
	{
		$sector = $player->getSector();
		return count($sector->getEnnemies()) > 0;
	}
	
	static function calculateSurvivors($player)
	{
		$survivors = 0;
		$ships = $player->getFleet()->getShips();
		foreach ($ships as $ship)
		{
			$survivors += $ship->getPassengers() + $ship->getStaff();
		}
		return $survivors;
	}
	
	static function generateCSRFToken()
	{
		global $entityManager;
		$token = new CSRF();
		$entityManager->persist($token);
		$entityManager->flush();
		return $token->getToken();
	}
	
	static function checkCSRF($token)
    {
		if(!is_null($token))
		{
			global $entityManager;
			$csrf = $entityManager->getRepository('CSRF')->find($token);
			return !is_null($csrf) && $csrf->isActive();
		}
		else
		{
			return false;
		}
    }
	
	static function checkBadges($player)
	{
		global $entityManager;
		$badges = $entityManager->getRepository('Badge')->findAll();
		foreach ($badges as $badge)
		{
			if ($badge->matchConditions($player))
			{
				if ($badge->getType() == 'player')
				{
					$player->addBadge($badge);
				}
				elseif ($badge->getType() == 'fleet')
				{
					// not implemented
				}
			}
		}
	}
	
	static function destroyShip($ship)
	{
		global $entityManager;
		$characters = $ship->getCharacters();
		foreach ($characters as $character)
		{
			$entityManager->remove($character);
		}
		$fleet = $ship->getFleet();
		$fleet->decreaseMoral(1);
	}
	
	
}
