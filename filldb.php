<?php
require_once __DIR__."/bootstrap.php";
require_once __DIR__."/tools.php";
require_once __DIR__."/const.php";

$game = $entityManager->getRepository('Game')->find(1);

if (is_null($game))
{
	
	$game = new Game();
	$game->init();
	$entityManager->persist($game);

// SHIP TYPES

$battlecruiser = new ShipType();
$battlecruiser->setName('ship.battlecruiser.name');
$battlecruiser->setAttack(200);
$battlecruiser->setDefense(500);
$battlecruiser->setMaxHP(2000);
$battlecruiser->setMaxPassengers(10000);
$battlecruiser->setQualifiedStaff(5000);
$battlecruiser->setFoodProduction(1);
$battlecruiser->setFuelProduction(1);
$battlecruiser->setMaterialProduction(1);
$battlecruiser->setMedicineProduction(1);
$battlecruiser->setMoralProduction(2);
$battlecruiser->setQualifiedStaffPerCycle(0);
$battlecruiser->setPrice(340000);
$battlecruiser->setNameDico('military');

$entityManager->persist($battlecruiser);

$transport = new ShipType();
$transport->setName('ship.transport.name');
$transport->setAttack(0);
$transport->setDefense(5);
$transport->setMaxHP(50);
$transport->setMaxPassengers(2000);
$transport->setQualifiedStaff(50);
$transport->setFoodProduction(0);
$transport->setFuelProduction(0);
$transport->setMaterialProduction(0);
$transport->setMedicineProduction(0);
$transport->setMoralProduction(0.1);
$transport->setQualifiedStaffPerCycle(1);
$transport->setPrice(50000);
$transport->setNameDico('civilians');

$entityManager->persist($transport);

$miningbarge = new ShipType();
$miningbarge->setName('ship.mining.barge.name');
$miningbarge->setAttack(0);
$miningbarge->setDefense(10);
$miningbarge->setMaxHP(200);
$miningbarge->setMaxPassengers(500);
$miningbarge->setQualifiedStaff(1000);
$miningbarge->setFoodProduction(0);
$miningbarge->setFuelProduction(0.1);
$miningbarge->setMaterialProduction(50);
$miningbarge->setMedicineProduction(0);
$miningbarge->setMoralProduction(0.1);
$miningbarge->setQualifiedStaffPerCycle(0);
$miningbarge->enableMining();
$miningbarge->setPrice(95000);
$miningbarge->setNameDico('civilians');

$entityManager->persist($miningbarge);

$ships = array(	array('name'=>'ship.refinery.name','attack'=>0,'defense'=>10,'hp'=>230,'passengers'=>10,'staff'=>750,'food'=>0,'fuel'=>3,'material'=>0,'medicine'=>0,'moral'=>0.1,'qstaffcycle'=>0,'special'=>'','price'=>270000,'dico'=>'civilians'),
				array('name'=>'ship.pleasure.boat.name','attack'=>0,'defense'=>2,'hp'=>100,'passengers'=>750,'staff'=>400,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>1,'qstaffcycle'=>0,'special'=>'','price'=>125000,'dico'=>'civilians'),
				array('name'=>'ship.research.lab.name','attack'=>0,'defense'=>10,'hp'=>50,'passengers'=>20,'staff'=>40,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0.5,'moral'=>0.2,'qstaffcycle'=>1,'special'=>'','price'=>35000,'dico'=>'civilians'),
				array('name'=>'ship.battle.hospital.name','attack'=>0,'defense'=>150,'hp'=>300,'passengers'=>150,'staff'=>300,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0.5,'moral'=>1,'qstaffcycle'=>1,'special'=>'','price'=>165000,'dico'=>'military'),
				array('name'=>'ship.frigate.name','attack'=>100,'defense'=>150,'hp'=>750,'passengers'=>100,'staff'=>500,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>1,'qstaffcycle'=>0,'special'=>'','price'=>180000,'dico'=>'military'),
				array('name'=>'ship.fighters.squad.name','attack'=>2,'defense'=>10,'hp'=>5,'passengers'=>0,'staff'=>5,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>0.5,'qstaffcycle'=>0,'special'=>'','price'=>3000,'dico'=>'fighters'),
				array('name'=>'ship.scout.name','attack'=>0,'defense'=>100,'hp'=>50,'passengers'=>0,'staff'=>30,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>0.2,'qstaffcycle'=>0,'special'=>'scout','price'=>3600,'dico'=>'military'),
				array('name'=>'ship.factory.name','attack'=>0,'defense'=>50,'hp'=>300,'passengers'=>1000,'staff'=>850,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>0.1,'qstaffcycle'=>0,'special'=>'produce','price'=>70000,'dico'=>'civilians'),
				array('name'=>'ship.repair.name','attack'=>0,'defense'=>60,'hp'=>200,'passengers'=>500,'staff'=>1200,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>0.1,'qstaffcycle'=>0,'special'=>'repair','price'=>23000,'dico'=>'civilians'),
				array('name'=>'ship.exploitation.name','attack'=>0,'defense'=>10,'hp'=>200,'passengers'=>300,'staff'=>120,'food'=>0,'fuel'=>0.5,'material'=>2,'medicine'=>0,'moral'=>0.1,'qstaffcycle'=>3,'special'=>'mining','price'=>14000,'dico'=>'civilians'),
				array('name'=>'ship.farm.name','attack'=>0,'defense'=>50,'hp'=>150,'passengers'=>800,'staff'=>80,'food'=>1,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>0.2,'qstaffcycle'=>0,'special'=>'','price'=>8000,'dico'=>'civilians'),
				array('name'=>'ship.corvette.name','attack'=>50,'defense'=>40,'hp'=>200,'passengers'=>50,'staff'=>100,'food'=>0,'fuel'=>0,'material'=>0,'medicine'=>0,'moral'=>0.1,'qstaffcycle'=>0,'special'=>'','price'=>60000,'dico'=>'military'),
				array('name'=>'ship.superdreadnough.invictus.name','attack'=>350,'defense'=>400,'hp'=>4500,'passengers'=>15000,'staff'=>7500,'food'=>1,'fuel'=>1,'material'=>1,'medicine'=>1,'moral'=>3,'qstaffcycle'=>0,'special'=>'','price'=>640000,'dico'=>'invictus','unique'=>true,'buildable'=>false),
				array('name'=>'ship.superdreadnough.imperator.name','attack'=>400,'defense'=>600,'hp'=>5000,'passengers'=>15000,'staff'=>8000,'food'=>1,'fuel'=>1,'material'=>1,'medicine'=>1,'moral'=>3,'qstaffcycle'=>0,'special'=>'','price'=>640000,'dico'=>'imperator','unique'=>true,'buildable'=>false),
				array('name'=>'ship.support.ship.name','attack'=>0,'defense'=>100,'hp'=>400,'passengers'=>150,'staff'=>500,'food'=>1,'fuel'=>1,'material'=>1,'medicine'=>1,'moral'=>0.1,'qstaffcycle'=>0,'special'=>'support','price'=>30000,'dico'=>'civilians','unique'=>false,'buildable'=>false)
				);
				
foreach($ships as $ship)
{
	$shipType = new ShipType();
	$shipType->setName($ship['name']);
	$shipType->setAttack($ship['attack']);
	$shipType->setDefense($ship['defense']);
	$shipType->setMaxHP($ship['hp']);
	$shipType->setMaxPassengers($ship['passengers']);
	$shipType->setQualifiedStaff($ship['staff']);
	$shipType->setFoodProduction($ship['food']);
	$shipType->setFuelProduction($ship['fuel']);
	$shipType->setMaterialProduction($ship['material']);
	$shipType->setMedicineProduction($ship['medicine']);
	$shipType->setMoralProduction($ship['moral']);
	$shipType->setQualifiedStaffPerCycle($ship['qstaffcycle']);
	$shipType->setPrice($ship['price']);
	$shipType->setNameDico($ship['dico']);
	
	if ($ship['special'] == 'scout')
	{
		$shipType->enableScout();
	}
	if ($ship['special'] == 'produce')
	{
		$shipType->enableProduceShips();
	}
	if ($ship['special'] == 'repair')
	{
		$shipType->enableRepair();
	}
	if ($ship['special'] == 'mining')
	{
		$shipType->enableMining();
	}
	if ($ship['special'] == 'support')
	{
		$shipType->enableRepair();
		$shipType->enableMining();
		$shipType->enableProduceShips();
	}
	
	if (array_key_exists('buildable',$ship))
	{
		$shipType->setBuildable($ship['buildable']);
	}
	
	if (array_key_exists('unique',$ship))
	{
		$shipType->setUnique($ship['unique']);
	}
	
	$entityManager->persist($shipType);
}

$entityManager->flush();

// ENNEMY SHIP TYPES

$ennemyships = array(	array('name'=>'ship.ennemy.cruiser.name','attack'=>100,'defense'=>100,'hp'=>300,'difficulty'=>2),
						array('name'=>'ship.ennemy.frigate.name','attack'=>60,'defense'=>60,'hp'=>100,'difficulty'=>1),
						array('name'=>'ship.ennemy.battlecruiser.name','attack'=>200,'defense'=>180,'hp'=>800,'difficulty'=>3),
						array('name'=>'ship.ennemy.light.frigate.name','attack'=>30,'defense'=>30,'hp'=>50,'difficulty'=>1),
						array('name'=>'ship.ennemy.heavy.fighter.squad.name','attack'=>20,'defense'=>10,'hp'=>20,'difficulty'=>1),
						array('name'=>'ship.ennemy.fighter.squad.name','attack'=>10,'defense'=>10,'hp'=>10,'difficulty'=>1),
						array('name'=>'ship.ennemy.mothership.name','attack'=>600,'defense'=>350,'hp'=>3000,'difficulty'=>4),
						array('name'=>'ship.ennemy.destroyer','attack'=>80,'defense'=>110,'hp'=>200,'difficulty'=>2),
						array('name'=>'ship.ennemy.patrol','attack'=>25,'defense'=>20,'hp'=>30,'difficulty'=>1),
						array('name'=>'ship.ennemy.ironclad','attack'=>250,'defense'=>200,'hp'=>1000,'difficulty'=>3)
					);

foreach ($ennemyships as $ship)
{
	$type = new EnnemyShipType();
	$type->setName($ship['name']);
	$type->setAttack($ship['attack']);
	$type->setDefense($ship['defense']);
	$type->setMaxHP($ship['hp']);
	$type->setDifficulty($ship['difficulty']);

	$entityManager->persist($type);
}

// EVENT TYPES

/* conditions format :
      * array (
      * 		"moral"=>array("min"=>INTEGER,"max"=>INTEGER),
      * 		"fuel"=>array("min"=>INTEGER,"max"=>INTEGER),
      * 		"food"=>array("min"=>INTEGER,"max"=>INTEGER),
      * 		"medicine"=>array("min"=>INTEGER,"max"=>INTEGER),
      * 		"material"=>array("min"=>INTEGER,"max"=>INTEGER),
      * 		"nbships"=>array("min"=INTEGER,"max"=>INTEGER),
      * 		"survivors"=>array("min"=>INTEGER,"max"=>INTEGER), // not implemented
      * 		"objective"=>INTEGER, // not implemented
      * 		"objectiveaims"=>array("min"=>INTEGER,"max"=>INTEGER) // not implemented
      * 		)
      */

/* answers format :
     * array (
     * 		x => array (
     * 					"name"=>"event.$id.answer.x",
     * 					"resultmsg"=>"msg.event.custom.answer.x", // nullable
     * 					"effects"=>array(
     * 									"moral"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"fuel"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"food"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"medicine"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"staff"=>array("min"=>INTEGER,"max"=>INTEGER), // concerns only staff stock
     * 									"material"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"passengers"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"ships"=>array("min"=>INTEGER,"max"=>INTEGER), // number of NEW ships
     * 									"ennemies"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"nbshipsdamaged"=>array("min"=>INTEGER,"max"=>INTEGER), // number of ships damaged, needs "damages"
     * 									"damages"=>array("min"=>INTEGER,"max"=>INTEGER),
     * 									"possibleambush"=>array("chance"=>INTEGER,"over"=>INTEGER,"minennemies"=>INTEGER,"maxennemies"=>INTEGER,"minallies"=>INTEGER,"maxallies"=>INTEGER), // has $chance over $over to be ennemies, $chance over $over to be allies, or nothing
     * 									)
     * 					)
     * 		)
     */

$events = array (
				array ('id'=>1,
						'name'=>'event.army.storage',
						'conditions'=>null,
						'answers'=>array(array('name'=>'event.answer.do.nothing',
												'resultmsg'=>'msg.event.do.nothing',
												'effects'=>array()
												),
										array('name'=>'event.1.answer.1',
												'resultmsg'=>'msg.event.get.storage.content',
												'effects'=>array(
																'fuel'=>array('min'=>0,'max'=>300),
																'medicine'=>array('min'=>0,'max'=>1000),
																'material'=>array('min'=>0,'max'=>10000)
																)
												)
											)
						),
				array ('id'=>2,
						'name'=>'event.distress.signal',
						'conditions'=>null,
						'answers'=>array(array('name'=>'event.answer.do.nothing',
												'resultmsg'=>'msg.event.do.nothing',
												'effects'=>array(
																'moral'=>array('min'=>-10,'max'=>-1)
																)
												),
										array('name'=>'event.2.answer.1',
												'resultmsg'=>null,
												'effects'=>array(
																'possibleambush'=>array('chance'=>10,'over'=>100,'minennemies'=>10,'maxennemies'=>20,'minallies'=>2,'maxallies'=>5),
																'moral'=>array('min'=>1,'max'=>5)
																)
												)
										)
						),
				array('id'=>3,
						'name'=>'event.rebellion',
						'conditions'=>array('moral'=>array('min'=>0,'max'=>30)),
						'answers'=>array(array('name'=>'event.answer.do.nothing',
												'resultmsg'=>null,
												'effects'=>array(
																'nbshipsdamaged'=>array('min'=>1,'max'=>10),
																'damages'=>array('min'=>10,'max'=>100),
																'moral'=>array('min'=>0,'max'=>1)
																)
												),
										array('name'=>'event.3.answer.1',
												'resultmsg'=>null,
												'effects'=>array(
																'passengers'=>array('min'=>-1000,'max'=>-150),
																'moral'=>array('min'=>-10,'max'=>-1)
																)
												),
										array('name'=>'event.3.answer.2',
												'resultmsg'=>null,
												'effects'=>array(
																'passengers'=>array('min'=>-5,'max'=>0),
																'moral'=>array('min'=>-1,'max'=>5)
																)
												)
										)
						),
				
				);
foreach ($events as $event)
{
	$eventType = new EventType($event['id'],$event['name'],$event['answers'],$event['conditions']);
	$entityManager->persist($eventType);
}

/* conditions format :
      * array (
      * 		"moral"=>INTEGER,
      * 		"fuel"=>INTEGER,
      * 		"food"=>INTEGER,
      * 		"medicine"=>INTEGER,
      * 		"material"=>INTEGER,
      * 		"nbships"=>INTEGER,
      * 		"survivors"=>INTEGER, // not implemented
      * 		"globalattack"=>INTEGER, // not implemented
      * 		"globaldefense"=>INTEGER, // not implemented
      * 		"killed"=>INTEGER,
      * 		"victoriousnewplanet"=>BOOLEAN,
      * 		"victoriousearth"=>BOOLEAN,
      * 		)
      */

$badges = array(
                array('id'=>1,
                      'name'=>'badge.angry',
                      'icon'=>'badge_angry',
                      'type'=>'player',
                      'conditions'=>array('killed'=>10)),
                array('id'=>2,
                      'name'=>'badge.justiciary',
                      'icon'=>'badge_justiciary',
                      'type'=>'player',
                      'conditions'=>array('killed'=>100)),
                array('id'=>3,
                      'name'=>'badge.arm.of.revenge',
                      'icon'=>'badge_arm_revenge',
                      'type'=>'player',
                      'conditions'=>array('killed'=>200)),
                array('id'=>4,
                      'name'=>'badge.toaster.recycler',
                      'icon'=>'badge_toaster_recycler',
                      'type'=>'player',
                      'conditions'=>array('killed'=>500)),
                array('id'=>5,
                      'name'=>'badge.fuel.finder',
                      'icon'=>'badge_fuel_finder',
                      'type'=>'player',
                      'conditions'=>array('fuel'=>500)),
                array('id'=>6,
                      'name'=>'badge.runner',
                      'icon'=>'badge_runner',
                      'type'=>'player',
                      'conditions'=>array('fuel'=>1000)),
                array('id'=>7,
                      'name'=>'badge.captain',
                      'icon'=>'badge_captain',
                      'type'=>'player',
                      'conditions'=>array('nbships'=>25)),
                array('id'=>8,
                      'name'=>'badge.commander',
                      'icon'=>'badge_commander',
                      'type'=>'player',
                      'conditions'=>array('nbships'=>40)),
                array('id'=>9,
                      'name'=>'badge.admiral',
                      'icon'=>'badge_admiral',
                      'type'=>'player',
                      'conditions'=>array('nbships'=>100)),
				array('id'=>10,
					  'name'=>'badge.new.hope',
					  'icon'=>'badge_new_hope',
					  'type'=>'player',
					  'conditions'=>array('victoriousnewplanet'=>true)),
				array('id'=>11,
					  'name'=>'badge.dear.ancestors',
					  'icon'=>'badge_dear_ancestors',
					  'type'=>'player',
					  'conditions'=>array('victoriousearth'=>true)),
                );

foreach ($badges as $badge)
{
    $badgeType = new Badge($badge['id'],$badge['name'],$badge['icon'],$badge['type'],$badge['conditions']);
    $entityManager->persist($badgeType);
}

// Political system

$systems = array(
                 array('name'=>'system.name.directorial.republic','liberty'=>65,'equality'=>60,'religion'=>'lbl.religion.none','election'=>true,'production'=>0.1,'moral'=>0,'defense'=>0.05,'attack'=>0,'corruption'=>0.07,'independantJustice'=>true,'popControl'=>20),
				 array('name'=>'system.name.theocracy','liberty'=>40,'equality'=>50,'religion'=>'lbl.religion.oracle','election'=>false,'production'=>-0.1,'moral'=>0.2,'defense'=>0.1,'attack'=>0.05,'corruption'=>0.1,'independantJustice'=>false,'popControl'=>60),
				 array('name'=>'system.name.representative.democracy','liberty'=>70,'equality'=>65,'religion'=>'lbl.religion.none','election'=>true,'production'=>0,'moral'=>0.1,'defense'=>0.1,'attack'=>0.06,'corruption'=>0.09,'independantJustice'=>true,'popControl'=>25),
				 array('name'=>'system.name.direct.democracy','liberty'=>90,'equality'=>80,'religion'=>'lbl.religion.none','election'=>true,'production'=>0.05,'moral'=>0.1,'defense'=>0.05,'attack'=>0.01,'corruption'=>0.01,'independantJustice'=>true,'popControl'=>10),
				 array('name'=>'system.name.capitalist.democracy','liberty'=>70,'equality'=>65,'religion'=>'lbl.religion.none','election'=>true,'production'=>0.1,'moral'=>-0.05,'defense'=>0.1,'attack'=>0.05,'corruption'=>0.15,'independantJustice'=>true,'popControl'=>40),
				 array('name'=>'system.name.capitalist.despotism','liberty'=>15,'equality'=>15,'religion'=>'lbl.religion.none','election'=>false,'production'=>0.3,'moral'=>-0.3,'defense'=>-0.1,'attack'=>0.15,'corruption'=>0.2,'independantJustice'=>false,'popControl'=>60),
				 array('name'=>'system.name.communism','liberty'=>80,'equality'=>100,'religion'=>'lbl.religion.none','election'=>true,'production'=>0.1,'moral'=>0,'defense'=>0.1,'attack'=>0.1,'corruption'=>0.1,'independantJustice'=>true,'popControl'=>20),
				 array('name'=>'system.name.stratocracy','liberty'=>25,'equality'=>50,'religion'=>'lbl.religion.none','election'=>false,'production'=>0.05,'moral'=>0,'defense'=>0.2,'attack'=>0.2,'corruption'=>0.1,'independantJustice'=>false,'popControl'=>70),
				 array('name'=>'system.name.military.dictatorship','liberty'=>5,'equality'=>5,'religion'=>'lbl.religion.none','election'=>false,'production'=>-0.15,'moral'=>-0.15,'defense'=>0.5,'attack'=>0.25,'corruption'=>0.2,'independantJustice'=>false,'popControl'=>90),
                 );

foreach ($systems as $system)
{
	$ps = new PoliticalSystem();
	$ps->setName($system['name']);
	$ps->setAttackBonus($system['attack']);
	$ps->setCorruptionBonus($system['corruption']);
	$ps->setDefenseBonus($system['defense']);
	$ps->setElection($system['election']);
	$ps->setEquality($system['equality']);
	$ps->setIndependantJustice($system['independantJustice']);
	$ps->setLiberty($system['liberty']);
	$ps->setMoralBonus($system['moral']);
	$ps->setPopulationControl($system['popControl']);
	$ps->setProductionBonus($system['production']);
	$ps->setReligion($system['religion']);
	$entityManager->persist($ps);
}

// PLANETS

$planets = array(
				 array('name'=>'Hame','production'=>20,'material'=>1500,'garrison'=>array(/*6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,*/1=>4/*,3=>3,10=>2,7=>1*/)),
				 array('name'=>'Anbus','production'=>5,'material'=>500,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Corrin','production'=>10,'material'=>800,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Noveburst','production'=>10,'material'=>800,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Canos','production'=>10,'material'=>800,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Cygni','production'=>15,'material'=>1000,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Caprica','production'=>20,'material'=>1500,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Bent','production'=>15,'material'=>1000,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Fuji','production'=>10,'material'=>800,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Dalaris','production'=>5,'material'=>500,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Harlan','production'=>2,'material'=>200,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Indi','production'=>2,'material'=>200,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Pacem','production'=>15,'material'=>1000,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Blackwood','production'=>10,'material'=>800,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Corvus','production'=>20,'material'=>1500,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Eriadu','production'=>10,'material'=>800,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Kuat','production'=>15,'material'=>1000,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Zio','production'=>15,'material'=>1000,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Vortex','production'=>15,'material'=>1000,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Altair','production'=>15,'material'=>1000,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Cartaga','production'=>5,'material'=>500,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Selena','production'=>5,'material'=>500,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Ardana','production'=>2,'material'=>200,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Dosaria','production'=>5,'material'=>500,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Mazar','production'=>10,'material'=>800,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Trenzalor','production'=>10,'material'=>800,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Sphinx','production'=>5,'material'=>500,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Aquaria','production'=>10,'material'=>800,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Nahilia','production'=>5,'material'=>500,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 array('name'=>'Verde','production'=>5,'material'=>500,'garrison'=>array(6=>20,5=>15,9=>10,4=>8,2=>6,8=>6,1=>4,3=>3,10=>2,7=>1)),
				 );

foreach ($planets as $planet)
{
	$p = new Planet($planet['name'],$planet['production']);
	$p->setMaterial($planet['material']);
	$p->setGarrison($planet['garrison']);
	$entityManager->persist($p);
}
				 
$entityManager->flush();

}
else
{
	echo "Game already set";
}
